#todo
~~- Export and View Course Site (WebView)~~
~~- Undo and Redo for all edits
  - compound text edits -> single text edit (via the actual undo/redo mechanism?)
    - if the last transaction is of the same class and the change is textual or no change is detected (which means a backspace + key + backspace or a key + backspace + key sequence occurred), don't register the new transaction (because each transaction works with a snapshot, this is analgous to merging them)
  - ditched compound text edits~~
~~- Add Lecture/Recitation/Lab~~
- Edit Lecture/Recitation/Lab
  - changes won't spawn schedule items
~~- Remove Lecture/Recitation/Lab~~
~~- Edit Start & End Dates~~
~~- Add Schedule Item~~
~~- Edit Schedule Item~~
~~- Remove Schedule Item~~
~~- saving/loading **meeting times**
  - saving/loading doesn't happen from the meeting times tab, it uses those events to create empty, classified schedule items (which won't fire, so what's the point?)~~
