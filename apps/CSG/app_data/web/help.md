#Course Site Generator Help
This application serves only to create and maintain course data in a web-friendly format.
##From the Splash Screen
Simply put, you've made it this far, so no explanation should be needed.
##Creating a New Project
1. Click the "New Project" icon (a folder icon containing '+')
2. Modify data
3. Click the "Save" icon (a floppy disk)
##Loading a Project
1. Click the "Load Project" icon (a folder)