package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.ScheduleItemPrototype;

/**
 *
 * @author unknown
 */
public class EditScheduleItem_Transaction implements jTPS_Transaction {
    CSGData data;
    ScheduleItemPrototype item, _new, old;
    
    public EditScheduleItem_Transaction(CSGData initData, ScheduleItemPrototype item, ScheduleItemPrototype _new) {
        data = initData;
        this.item = item;
        this._new = _new;
        old = new ScheduleItemPrototype();
        old.load(item);
    }

    @Override
    public void doTransaction() {
        item.load(_new);
        _force_refresh();
    }

    @Override
    public void undoTransaction() {
        item.load(old);
        _force_refresh();
    }
    
    /* use a bind-compatible operation to force the refresh operation (simply setting the property values won't do this) */
    private void _force_refresh() {
        ScheduleItemPrototype i = new ScheduleItemPrototype();
        data.addScheduleItem(i);
        data.removeScheduleItem(i);
    }
}
