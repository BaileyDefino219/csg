package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.LectureDetails;

/**
 *
 * @author unknown
 */
public class AddLecture_Transaction implements jTPS_Transaction {
    CSGData data;
    LectureDetails lecture;
    
    public AddLecture_Transaction(CSGData initData, LectureDetails lecture) {
        data = initData;
        this.lecture = lecture;
    }

    @Override
    public void doTransaction() {
        data.addLecture(lecture);
    }

    @Override
    public void undoTransaction() {
        data.removeLecture(lecture);
    }
}
