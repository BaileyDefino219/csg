package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.RecitationDetails;

/**
 *
 * @author unknown
 */
public class AddRecitation_Transaction implements jTPS_Transaction {
    CSGData data;
    RecitationDetails o;
    
    public AddRecitation_Transaction(CSGData initData, RecitationDetails o) {
        data = initData;
        this.o = o;
    }

    @Override
    public void doTransaction() {
        data.addRecitation(o);
    }

    @Override
    public void undoTransaction() {
        data.removeRecitation(o);
    }
}
