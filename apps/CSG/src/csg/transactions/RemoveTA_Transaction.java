package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.*;
import static csg.data.TimeSlot.DayOfWeek;

/**
 *
 * @author McKillaGorilla
 */
public class RemoveTA_Transaction implements jTPS_Transaction {
    CSGData data;
    TeachingAssistantPrototype ta;
    java.util.ArrayList<TimeSlot> slots;
    
    public RemoveTA_Transaction(CSGData initData, TeachingAssistantPrototype initTA) {
        data = initData;
        ta = initTA;
        slots = new java.util.ArrayList<>();
    }

    @Override
    public void doTransaction() {
        slots.clear();
        java.util.Iterator<TimeSlot> iter = data.officeHoursIterator();
        while (iter.hasNext()) {
            TimeSlot s = iter.next();
            TimeSlot _s = new TimeSlot(s.getStartTime(), s.getEndTime());
            
            for (DayOfWeek d : DayOfWeek.values()) {
                if (s.hasTA(ta, d))
                    _s.toggleTA(d, ta);
            }
            slots.add(_s);
        }
        data.removeTA(ta);
    }

    @Override
    public void undoTransaction() {
        data.addTA(ta);
        
        for (TimeSlot s : slots) {
            TimeSlot _s = data.getTimeSlot(s.getStartTime());
            
            for (DayOfWeek d : DayOfWeek.values()) {
                if (s.hasTA(ta, d))
                    _s.toggleTA(d, ta);
            }
        }
    }
}
