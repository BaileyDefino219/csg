package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.ScheduleItemPrototype;

/**
 *
 * @author unknown
 */
public class AddScheduleItem_Transaction implements jTPS_Transaction {
    CSGData data;
    ScheduleItemPrototype o;
    
    public AddScheduleItem_Transaction(CSGData initData, ScheduleItemPrototype o) {
        data = initData;
        this.o = o;
    }

    @Override
    public void doTransaction() {
        data.addScheduleItem(o);
    }

    @Override
    public void undoTransaction() {
        data.removeScheduleItem(o);
    }
}
