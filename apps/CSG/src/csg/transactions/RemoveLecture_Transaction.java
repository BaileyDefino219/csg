package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.LectureDetails;

/**
 *
 * @author unknown
 */
public class RemoveLecture_Transaction implements jTPS_Transaction {
    BulkAddScheduleItem_Transaction bulk;
    CSGData data;
    LectureDetails o;
    
    public RemoveLecture_Transaction(CSGData initData, LectureDetails o) {
        this.bulk = new BulkAddScheduleItem_Transaction(data, (java.util.List) o.children.clone()); // prep bulk addition/removal of children
        data = initData;
        this.o = o;
    }

    @Override
    public void doTransaction() {
        bulk.undoTransaction();
        data.removeLecture(o);
    }

    @Override
    public void undoTransaction() {
        data.addLecture(o);
        bulk.doTransaction();
    }
}