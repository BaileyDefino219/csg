package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.LabDetails;

/**
 *
 * @author unknown
 */
public class RemoveLab_Transaction implements jTPS_Transaction {
    BulkAddScheduleItem_Transaction bulk;
    CSGData data;
    LabDetails o;
    
    public RemoveLab_Transaction(CSGData initData, LabDetails o) {
        this.bulk = new BulkAddScheduleItem_Transaction(data, (java.util.List) o.children.clone()); // prep bulk addition/removal of children
        data = initData;
        this.o = o;
    }

    @Override
    public void doTransaction() {
        bulk.undoTransaction();
        data.removeLab(o);
    }

    @Override
    public void undoTransaction() {
        data.addLab(o);
        bulk.doTransaction();
    }
}