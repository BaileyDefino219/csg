package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.LabDetails;

/**
 *
 * @author unknown
 */
public class AddLab_Transaction implements jTPS_Transaction {
    CSGData data;
    LabDetails o;
    
    public AddLab_Transaction(CSGData initData, LabDetails o) {
        data = initData;
        this.o = o;
    }

    @Override
    public void doTransaction() {
        data.addLab(o);
    }

    @Override
    public void undoTransaction() {
        data.removeLab(o);
    }
}
