package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.ScheduleItemPrototype;
import java.util.List;
/**
 *
 * @author unknown
 */
public class BulkAddScheduleItem_Transaction implements jTPS_Transaction {
    CSGData data;
    List<ScheduleItemPrototype> o;
    
    public BulkAddScheduleItem_Transaction(CSGData initData, List<ScheduleItemPrototype> o) {
        data = initData;
        this.o = o;
    }

    @Override
    public void doTransaction() {
        for (ScheduleItemPrototype i : o)
            data.addScheduleItem(i);
    }

    @Override
    public void undoTransaction() {
        for (ScheduleItemPrototype i : o)
            data.removeScheduleItem(i);
    }
}
