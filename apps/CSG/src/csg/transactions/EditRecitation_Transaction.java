package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.DOWTranslator;
import csg.data.RecitationDetails;
import csg.data.ScheduleItemPrototype;
import java.time.DayOfWeek;
import java.time.LocalDate;
import csg.transactions.BulkAddScheduleItem_Transaction;
/**
 *
 * @author unknown
 */
public class EditRecitation_Transaction implements jTPS_Transaction {
    BulkAddScheduleItem_Transaction bulk; // handles children spawned into the schedule
    CSGData data;
    RecitationDetails o, _new, _old;
    
    public EditRecitation_Transaction(CSGData initData, RecitationDetails o) {
        data = initData;
        this.o = o;
        _new = new RecitationDetails();
        _new.load(o);
        _old = new RecitationDetails();
        _old.load(o);
        if (_new.children.size() == 0) { // generate children
            LocalDate cur = LocalDate.parse(data.startingMonday);
            LocalDate end = LocalDate.parse(data.endingFriday);
            DayOfWeek[] days = DOWTranslator.timestring2days(DOWTranslator.extractDays(_new.daysTime.get()));
            while (days.length > 0 && !cur.isAfter(end)) {
                for (DayOfWeek day : days) {
                    cur = DOWTranslator.next(cur, day); // increment as needed
                    if (cur.isAfter(end)) // overstepped
                        break;
                    ScheduleItemPrototype i = new ScheduleItemPrototype();
                    i.date.set(cur.toString());
                    i.type.set("Recitation");
                    _new.children.add(i);
                }
            }
        }
        this.bulk = new BulkAddScheduleItem_Transaction(data, (java.util.List) _new.children.clone()); // prep bulk addition/removal of children
    }

    @Override
    public void doTransaction() {
        o.load(_new);
        bulk.doTransaction();
    }

    @Override
    public void undoTransaction() {
        bulk.undoTransaction();
        o.load(_old);
    }
}
