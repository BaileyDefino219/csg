package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.PageDetails;

/**
 *
 * @author McKillaGorilla
 */
public class EditPageDetails_Transaction implements jTPS_Transaction {
    Object chooserButtonID;
    String path;
    CSGData data;
    PageDetails details;
    
    public EditPageDetails_Transaction(CSGData initData, Object chooserButtonID, String path) {
        data = initData;
        details = new PageDetails();
        this.path  = path;
        this.chooserButtonID = chooserButtonID;
    }

    @Override
    public void doTransaction() {
        details.load(data.getPageDetails());
        data.editPageDetails(chooserButtonID, path);        
    }

    @Override
    public void undoTransaction() {
        data.getPageDetails().load(details);
        data.updatePageDetails();
    }
}
