package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.SyllabusDetails;

/**
 *
 * @author McKillaGorilla
 */
public class EditSyllabusDetails_Transaction implements jTPS_Transaction {
    Object chooserButtonID;
    String path;
    CSGData data;
    SyllabusDetails details;
    
    public EditSyllabusDetails_Transaction(CSGData initData) {
        data = initData;
        details = new SyllabusDetails();
    }

    @Override
    public void doTransaction() {
        details.load(data.getSyllabusDetails());
        data.editSyllabusDetails();
    }

    @Override
    public void undoTransaction() {
        data.getSyllabusDetails().load(details);
        data.updateSyllabusDetails();
    }
}
