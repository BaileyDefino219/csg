package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.ScheduleItemPrototype;

/**
 *
 * @author unknown
 */
public class RemoveScheduleItem_Transaction implements jTPS_Transaction {
    CSGData data;
    ScheduleItemPrototype o;
    
    public RemoveScheduleItem_Transaction(CSGData initData, ScheduleItemPrototype o) {
        data = initData;
        this.o = o;
    }

    @Override
    public void doTransaction() {
        data.removeScheduleItem(o);
    }

    @Override
    public void undoTransaction() {
        data.addScheduleItem(o);
    }
}
