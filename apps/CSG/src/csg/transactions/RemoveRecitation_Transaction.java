package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.RecitationDetails;

/**
 *
 * @author unknown
 */
public class RemoveRecitation_Transaction implements jTPS_Transaction {
    BulkAddScheduleItem_Transaction bulk;
    CSGData data;
    RecitationDetails o;
    
    public RemoveRecitation_Transaction(CSGData initData, RecitationDetails o) {
        this.bulk = new BulkAddScheduleItem_Transaction(data, (java.util.List) o.children.clone()); // prep bulk addition/removal of children
        data = initData;
        this.o = o;
    }

    @Override
    public void doTransaction() {
        bulk.undoTransaction();
        data.removeRecitation(o);
    }

    @Override
    public void undoTransaction() {
        data.addRecitation(o);
        bulk.doTransaction();
    }
}