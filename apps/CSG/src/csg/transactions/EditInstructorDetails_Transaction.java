package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;
import csg.data.InstructorDetails;

/**
 *
 * @author McKillaGorilla
 */
public class EditInstructorDetails_Transaction implements jTPS_Transaction {
    CSGData data;
    InstructorDetails details;
    
    public EditInstructorDetails_Transaction(CSGData initData) {
        data = initData;
        details = new InstructorDetails();
    }

    @Override
    public void doTransaction() {
        details.load(data.getInstructorDetails());
        data.editInstructorDetails();
    }

    @Override
    public void undoTransaction() {
        data.getInstructorDetails().load(details);
        data.updateInstructorDetails();
    }
}
