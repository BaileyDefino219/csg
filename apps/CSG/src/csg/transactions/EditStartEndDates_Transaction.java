package csg.transactions;

import jtps.jTPS_Transaction;
import csg.data.CSGData;

/**
 *
 * @author unknown
 */
public class EditStartEndDates_Transaction implements jTPS_Transaction {
    CSGData data;
    String[] _new, old;
    
    public EditStartEndDates_Transaction(CSGData initData, String startingMonday, String endingFriday) {
        data = initData;
        _new = new String[]{startingMonday, endingFriday};
        old = new String[]{data.startingMonday, data.endingFriday};
    }

    @Override
    public void doTransaction() {
        data.startingMonday = _new[0];
        data.endingFriday = _new[1];
        data.updateStartEndDates();
    }

    @Override
    public void undoTransaction() {
        data.startingMonday = old[0];
        data.startingMonday = old[1];
        data.updateStartEndDates();
    }
}
