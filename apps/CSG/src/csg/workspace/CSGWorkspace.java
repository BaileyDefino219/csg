package csg.workspace;
import java.lang.reflect.Field;
import csg.data.*;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import djf.AppTemplate;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.AppNodesBuilder;
import java.io.File;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import java.time.DayOfWeek;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import java.util.Date;
import properties_manager.PropertiesManager;
import csg.CSGApp;
import csg.CSGPropertyType;
import static csg.CSGPropertyType.*;
import csg.data.TeachingAssistantPrototype;
import csg.data.TimeSlot;
import static csg.files.CSGFiles.STATE;
import csg.workspace.controllers.CSGController;
import csg.workspace.dialogs.TADialog;
import csg.workspace.foolproof.CSGFoolproofDesign;
import static csg.workspace.style.CSGStyle.*;
import java.io.IOException;
import java.time.LocalDate;
import javafx.scene.control.DatePicker;

/**
 *
 * @author McKillaGorilla
 */
public class CSGWorkspace extends AppWorkspaceComponent {

    public CSGWorkspace(CSGApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();

        // INIT THE EVENT HANDLERS
        initControllers();

        // 
        initFoolproofDesign();

        // INIT DIALOGS
        initDialogs();
    }

    private void initDialogs() {
        TADialog taDialog = new TADialog((CSGApp) app);
        app.getGUIModule().addDialog(CSG_TA_EDIT_DIALOG, taDialog);
    }

    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder csgBuilder = app.getGUIModule().getNodesBuilder();
        
        // TAB PANE
        
        TabPane tabPane = csgBuilder.buildTabPane(CSG_TAB_PANE, null, CLASS_CSG_TAB_PANE, ENABLED);
        Tab siteTab = csgBuilder.buildTab(CSG_SITE_TAB, tabPane, CLASS_CSG_TAB, ENABLED);
        Tab syllabusTab = csgBuilder.buildTab(CSG_SYLLABUS_TAB, tabPane, CLASS_CSG_TAB, ENABLED);
        Tab meetingTimesTab = csgBuilder.buildTab(CSG_MEETING_TIMES_TAB, tabPane, CLASS_CSG_TAB, ENABLED);
        Tab officeHoursTab = csgBuilder.buildTab(CSG_OFFICE_HOURS_TAB, tabPane, CLASS_CSG_TAB, ENABLED);
        Tab scheduleTab = csgBuilder.buildTab(CSG_SCHEDULE_TAB, tabPane, CLASS_CSG_TAB, ENABLED);
        
        tabPane.getTabs().addAll(siteTab, syllabusTab, meetingTimesTab, officeHoursTab, scheduleTab);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        
        // SITE TAB
        SplitPane siteSplitPane = csgBuilder.buildSplitPane(CSG_SITE_SPLIT_PANE, null, CLASS_CSG_PANE, ENABLED);
        siteSplitPane.setDividerPositions(.4);
        siteSplitPane.setOrientation(Orientation.VERTICAL);
        siteTab.setContent(new ScrollPane(siteSplitPane));
        
        VBox siteSplitPaneBannerVBox = csgBuilder.buildVBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox siteSplitPanePagesHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_PAGES_HBOX, null, CLASS_CSG_PANE, ENABLED);
        VBox siteSplitPaneStyleVBox = csgBuilder.buildVBox(CSG_SITE_SPLIT_PANE_STYLE_VBOX, null, CLASS_CSG_PANE, ENABLED);
        VBox siteSplitPaneInstructorVBox = csgBuilder.buildVBox(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX, null, CLASS_CSG_PANE, ENABLED);
        siteSplitPane.getItems().addAll(siteSplitPaneBannerVBox, siteSplitPanePagesHBox, siteSplitPaneStyleVBox, siteSplitPaneInstructorVBox);
        
        // site banner vbox
        VBox siteSplitPaneBannerVBoxVBox = csgBuilder.buildVBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX, siteSplitPaneBannerVBox, CLASS_CSG_PANE, ENABLED);
        Label siteSplitPaneBannerVBoxVBoxLabel = csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_LABEL, siteSplitPaneBannerVBoxVBox, CLASS_CSG_HEADER_LABEL, ENABLED); // "Banner"
        GridPane siteSplitPaneBannerVBoxVBoxGridPane = csgBuilder.buildGridPane(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE, siteSplitPaneBannerVBoxVBox, CLASS_CSG_PANE, ENABLED);

        HBox siteSplitPaneBannerVBoxVBoxGridPaneSubjectHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SUBJECT_HBOX,
                siteSplitPaneBannerVBoxVBoxGridPane, CLASS_CSG_PANE, ENABLED);
        siteSplitPaneBannerVBoxVBoxGridPane.setConstraints(siteSplitPaneBannerVBoxVBoxGridPaneSubjectHBox, 0, 0);
        Label siteSplitPaneBannerVBoxVBoxGridPaneSubjectHBoxLabel = csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SUBJECT_HBOX_LABEL,
                siteSplitPaneBannerVBoxVBoxGridPaneSubjectHBox, CLASS_CSG_LABEL, ENABLED);
        ComboBox<String> siteSplitPaneBannerVBoxVBoxGridPaneSubjectHBoxComboBox = csgBuilder.buildComboBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SUBJECT_HBOX_COMBOBOX,
                CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SUBJECT_HBOX_COMBOBOX_OPTIONS, CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SUBJECT_HBOX_COMBOBOX_DEFAULT,
                siteSplitPaneBannerVBoxVBoxGridPaneSubjectHBox, CLASS_CSG_COMBOBOX, ENABLED);

        HBox siteSplitPaneBannerVBoxVBoxGridPaneNumberHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_NUMBER_HBOX,
                siteSplitPaneBannerVBoxVBoxGridPane, CLASS_CSG_PANE, ENABLED);
        siteSplitPaneBannerVBoxVBoxGridPane.setConstraints(siteSplitPaneBannerVBoxVBoxGridPaneNumberHBox, 1, 0);
        Label siteSplitPaneBannerVBoxVBoxGridPaneNumberHBoxLabel = csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_NUMBER_HBOX_LABEL,
                siteSplitPaneBannerVBoxVBoxGridPaneNumberHBox, CLASS_CSG_LABEL, ENABLED);
        ComboBox<String> siteSplitPaneBannerVBoxVBoxGridPaneNumberHBoxComboBox = csgBuilder.buildComboBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_NUMBER_HBOX_COMBOBOX,
                CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_NUMBER_HBOX_COMBOBOX_OPTIONS, CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_NUMBER_HBOX_COMBOBOX_DEFAULT,
                siteSplitPaneBannerVBoxVBoxGridPaneNumberHBox, CLASS_CSG_COMBOBOX, ENABLED);
        
        HBox siteSplitPaneBannerVBoxVBoxGridPaneSemesterHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SEMESTER_HBOX,
                siteSplitPaneBannerVBoxVBoxGridPane, CLASS_CSG_PANE, ENABLED);
        siteSplitPaneBannerVBoxVBoxGridPane.setConstraints(siteSplitPaneBannerVBoxVBoxGridPaneSemesterHBox, 0, 1);
        Label siteSplitPaneBannerVBoxVBoxGridPaneSemesterHBoxLabel = csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SEMESTER_HBOX_LABEL,
                siteSplitPaneBannerVBoxVBoxGridPaneSemesterHBox, CLASS_CSG_LABEL, ENABLED);
        ComboBox<String> siteSplitPaneBannerVBoxVBoxGridPaneSemesterHBoxComboBox = csgBuilder.buildComboBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SEMESTER_HBOX_COMBOBOX,
                CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SEMESTER_HBOX_COMBOBOX_OPTIONS, CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SEMESTER_HBOX_COMBOBOX_DEFAULT,
                siteSplitPaneBannerVBoxVBoxGridPaneSemesterHBox, CLASS_CSG_COMBOBOX, ENABLED);
        
        HBox siteSplitPaneBannerVBoxVBoxGridPaneYearHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_YEAR_HBOX,
                siteSplitPaneBannerVBoxVBoxGridPane, CLASS_CSG_PANE, ENABLED);
        siteSplitPaneBannerVBoxVBoxGridPane.setConstraints(siteSplitPaneBannerVBoxVBoxGridPaneYearHBox, 1, 1);
        Label siteSplitPaneBannerVBoxVBoxGridPaneYearHBoxLabel = csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_YEAR_HBOX_LABEL,
                siteSplitPaneBannerVBoxVBoxGridPaneYearHBox, CLASS_CSG_LABEL, ENABLED);
        ComboBox<String> siteSplitPaneBannerVBoxVBoxGridPaneYearHBoxComboBox = csgBuilder.buildComboBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_YEAR_HBOX_COMBOBOX,
                CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_YEAR_HBOX_COMBOBOX_OPTIONS, CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_YEAR_HBOX_COMBOBOX_DEFAULT,
                siteSplitPaneBannerVBoxVBoxGridPaneYearHBox, CLASS_CSG_COMBOBOX, ENABLED);
        String dateString = (new Date()).toString();
        int year = Integer.parseInt(dateString.substring(dateString.length() - 4));
        siteSplitPaneBannerVBoxVBoxGridPaneYearHBoxComboBox.getSelectionModel().select((new Integer(year)).toString());
        
        for (int i = 0; i < 10; i++) // add the next 5 years
          siteSplitPaneBannerVBoxVBoxGridPaneYearHBoxComboBox.getItems().add((new Integer(year + i)).toString());
        
        HBox siteSplitPaneBannerVBoxVBoxTitleHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_TITLE_HBOX, siteSplitPaneBannerVBoxVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_TITLE_HBOX_LABEL, siteSplitPaneBannerVBoxVBoxTitleHBox, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_TITLE_HBOX_TEXT_FIELD, siteSplitPaneBannerVBoxVBoxTitleHBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        HBox siteSplitPaneBannerVBoxVBoxExportDirHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_EXPORT_DIR_HBOX, siteSplitPaneBannerVBoxVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_EXPORT_DIR_HBOX_LABEL, siteSplitPaneBannerVBoxVBoxExportDirHBox, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_EXPORT_DIR_HBOX_PATH_LABEL, siteSplitPaneBannerVBoxVBoxExportDirHBox, CLASS_CSG_LABEL, ENABLED);

        // site pages hbox
        
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_PAGES_HBOX_LABEL, siteSplitPanePagesHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildCheckBox(CSG_SITE_SPLIT_PANE_PAGES_HBOX_HOME_CHECKBOX, siteSplitPanePagesHBox, CLASS_CSG_CHECKBOX, ENABLED);
        csgBuilder.buildCheckBox(CSG_SITE_SPLIT_PANE_PAGES_HBOX_HWS_CHECKBOX, siteSplitPanePagesHBox, CLASS_CSG_CHECKBOX, ENABLED);
        csgBuilder.buildCheckBox(CSG_SITE_SPLIT_PANE_PAGES_HBOX_SCHEDULE_CHECKBOX, siteSplitPanePagesHBox, CLASS_CSG_CHECKBOX, ENABLED);
        csgBuilder.buildCheckBox(CSG_SITE_SPLIT_PANE_PAGES_HBOX_SYLLABUS_CHECKBOX, siteSplitPanePagesHBox, CLASS_CSG_CHECKBOX, ENABLED);

        // site style vbox
        
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_STYLE_VBOX_LABEL, siteSplitPaneStyleVBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        HBox siteSplitPaneStyleVBoxFaviconHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_STYLE_VBOX_FAVICON_HBOX, siteSplitPaneStyleVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SITE_SPLIT_PANE_STYLE_VBOX_FAVICON_HBOX_BUTTON, siteSplitPaneStyleVBoxFaviconHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildImageView(CSG_SITE_SPLIT_PANE_STYLE_VBOX_FAVICON_HBOX_IMAGE, siteSplitPaneStyleVBoxFaviconHBox, CLASS_CSG_IMAGE, ENABLED);
        ///////////////////////////////////////////////////////////////////////////////////////////////icon won't load
        
        HBox siteSplitPaneStyleVBoxNavbarHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_STYLE_VBOX_NAVBAR_HBOX, siteSplitPaneStyleVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SITE_SPLIT_PANE_STYLE_VBOX_NAVBAR_HBOX_BUTTON, siteSplitPaneStyleVBoxNavbarHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildImageView(CSG_SITE_SPLIT_PANE_STYLE_VBOX_NAVBAR_HBOX_IMAGE, siteSplitPaneStyleVBoxNavbarHBox, CLASS_CSG_IMAGE, ENABLED);
        
        HBox siteSplitPaneStyleVBoxLeftFooterHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_STYLE_VBOX_LEFT_FOOTER_HBOX, siteSplitPaneStyleVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SITE_SPLIT_PANE_STYLE_VBOX_LEFT_FOOTER_HBOX_BUTTON, siteSplitPaneStyleVBoxLeftFooterHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildImageView(CSG_SITE_SPLIT_PANE_STYLE_VBOX_LEFT_FOOTER_HBOX_IMAGE, siteSplitPaneStyleVBoxLeftFooterHBox, CLASS_CSG_IMAGE, ENABLED);
        
        HBox siteSplitPaneStyleVBoxRightFooterHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_STYLE_VBOX_RIGHT_FOOTER_HBOX, siteSplitPaneStyleVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SITE_SPLIT_PANE_STYLE_VBOX_RIGHT_FOOTER_HBOX_BUTTON, siteSplitPaneStyleVBoxRightFooterHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildImageView(CSG_SITE_SPLIT_PANE_STYLE_VBOX_RIGHT_FOOTER_HBOX_IMAGE, siteSplitPaneStyleVBoxRightFooterHBox, CLASS_CSG_IMAGE, ENABLED);
        
        HBox siteSplitPaneStyleVBoxStyleSheetHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_STYLE_VBOX_STYLESHEET_HBOX, siteSplitPaneStyleVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_STYLE_VBOX_STYLESHEET_HBOX_LABEL, siteSplitPaneStyleVBoxStyleSheetHBox, CLASS_CSG_LABEL, ENABLED);
        
        ComboBox box = csgBuilder.buildComboBox(CSG_SITE_SPLIT_PANE_STYLE_VBOX_STYLESHEET_HBOX_CHOOSER, CSG_SITE_SPLIT_PANE_STYLE_VBOX_STYLESHEET_HBOX_CHOOSER_OPTIONS,
                CSG_SITE_SPLIT_PANE_STYLE_VBOX_STYLESHEET_HBOX_CHOOSER_DEFAULT, siteSplitPaneStyleVBoxStyleSheetHBox, CLASS_CSG_TEXT_FIELD, ENABLED);

        // site instructor vbox
        
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_LABEL, siteSplitPaneInstructorVBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        GridPane siteSplitPaneInstructorVBoxGridPane = csgBuilder.buildGridPane(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE, siteSplitPaneInstructorVBox, CLASS_CSG_PANE, ENABLED);
        
        HBox siteSplitPaneInstructorVBoxGridPaneNameHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_NAME_HBOX, siteSplitPaneInstructorVBoxGridPane, CLASS_CSG_PANE, ENABLED);
        siteSplitPaneInstructorVBoxGridPane.setConstraints(siteSplitPaneInstructorVBoxGridPaneNameHBox, 0, 0);
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_NAME_HBOX_LABEL, siteSplitPaneInstructorVBoxGridPaneNameHBox, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_NAME_HBOX_TEXT_FIELD, siteSplitPaneInstructorVBoxGridPaneNameHBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        HBox siteSplitPaneInstructorVBoxGridPaneRoomHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_ROOM_HBOX, siteSplitPaneInstructorVBoxGridPane, CLASS_CSG_PANE, ENABLED);
        siteSplitPaneInstructorVBoxGridPane.setConstraints(siteSplitPaneInstructorVBoxGridPaneRoomHBox, 1, 0);
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_ROOM_HBOX_LABEL, siteSplitPaneInstructorVBoxGridPaneRoomHBox, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_ROOM_HBOX_TEXT_FIELD, siteSplitPaneInstructorVBoxGridPaneRoomHBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        HBox siteSplitPaneInstructorVBoxGridPaneEmailHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_EMAIL_HBOX, siteSplitPaneInstructorVBoxGridPane, CLASS_CSG_PANE, ENABLED);
        siteSplitPaneInstructorVBoxGridPane.setConstraints(siteSplitPaneInstructorVBoxGridPaneEmailHBox, 0, 1);
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_EMAIL_HBOX_LABEL, siteSplitPaneInstructorVBoxGridPaneEmailHBox, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_EMAIL_HBOX_TEXT_FIELD, siteSplitPaneInstructorVBoxGridPaneEmailHBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        HBox siteSplitPaneInstructorVBoxGridPaneWebsiteHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_WEBSITE_HBOX, siteSplitPaneInstructorVBoxGridPane, CLASS_CSG_PANE, ENABLED);
        siteSplitPaneInstructorVBoxGridPane.setConstraints(siteSplitPaneInstructorVBoxGridPaneWebsiteHBox, 1, 1);
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_WEBSITE_HBOX_LABEL, siteSplitPaneInstructorVBoxGridPaneWebsiteHBox, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildTextField(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_WEBSITE_HBOX_TEXT_FIELD, siteSplitPaneInstructorVBoxGridPaneWebsiteHBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        VBox siteSplitPaneInstructorVBoxOfficeHoursVBox = csgBuilder.buildVBox(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_OFFICE_HOURS_VBOX, siteSplitPaneInstructorVBox, CLASS_CSG_PANE, ENABLED);
        HBox siteSplitPaneInstructorVBoxOfficeHoursVBoxTopHBox = csgBuilder.buildHBox(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_OFFICE_HOURS_VBOX_TOP_HBOX, siteSplitPaneInstructorVBoxOfficeHoursVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_OFFICE_HOURS_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON, siteSplitPaneInstructorVBoxOfficeHoursVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_OFFICE_HOURS_VBOX_TOP_HBOX_LABEL, siteSplitPaneInstructorVBoxOfficeHoursVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_OFFICE_HOURS_VBOX_TEXT_AREA, siteSplitPaneInstructorVBoxOfficeHoursVBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        // SYLLABUS TAB
        
        SplitPane syllabusSplitPane = csgBuilder.buildSplitPane(CSG_SYLLABUS_SPLIT_PANE, null, CLASS_CSG_PANE, ENABLED);
        syllabusSplitPane.setDividerPositions(.4);
        syllabusSplitPane.setOrientation(Orientation.VERTICAL);
        syllabusTab.setContent(new ScrollPane(syllabusSplitPane));
        
        VBox syllabusSplitPaneDescriptionVBox = csgBuilder.buildVBox(CSG_SYLLABUS_SPLIT_PANE_DESCRIPTION_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox syllabusSplitPaneDescriptionVBoxTopHBox = csgBuilder.buildHBox(CSG_SYLLABUS_SPLIT_PANE_DESCRIPTION_VBOX_TOP_HBOX, syllabusSplitPaneDescriptionVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SYLLABUS_SPLIT_PANE_DESCRIPTION_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON, syllabusSplitPaneDescriptionVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_SYLLABUS_SPLIT_PANE_DESCRIPTION_VBOX_TOP_HBOX_LABEL, syllabusSplitPaneDescriptionVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYLLABUS_SPLIT_PANE_DESCRIPTION_VBOX_TEXT_AREA, syllabusSplitPaneDescriptionVBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        VBox syllabusSplitPaneTopicsVBox = csgBuilder.buildVBox(CSG_SYLLABUS_SPLIT_PANE_TOPICS_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox syllabusSplitPaneTopicsVBoxTopHBox = csgBuilder.buildHBox(CSG_SYLLABUS_SPLIT_PANE_TOPICS_VBOX_TOP_HBOX, syllabusSplitPaneTopicsVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SYLLABUS_SPLIT_PANE_TOPICS_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON, syllabusSplitPaneTopicsVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_SYLLABUS_SPLIT_PANE_TOPICS_VBOX_TOP_HBOX_LABEL, syllabusSplitPaneTopicsVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYLLABUS_SPLIT_PANE_TOPICS_VBOX_TEXT_AREA, syllabusSplitPaneTopicsVBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        VBox syllabusSplitPanePrereqsVBox = csgBuilder.buildVBox(CSG_SYLLABUS_SPLIT_PANE_PREREQS_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox syllabusSplitPanePrereqsVBoxTopHBox = csgBuilder.buildHBox(CSG_SYLLABUS_SPLIT_PANE_PREREQS_VBOX_TOP_HBOX, syllabusSplitPanePrereqsVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SYLLABUS_SPLIT_PANE_PREREQS_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON, syllabusSplitPanePrereqsVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_SYLLABUS_SPLIT_PANE_PREREQS_VBOX_TOP_HBOX_LABEL, syllabusSplitPanePrereqsVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYLLABUS_SPLIT_PANE_PREREQS_VBOX_TEXT_AREA, syllabusSplitPanePrereqsVBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        VBox syllabusSplitPaneOutcomesVBox = csgBuilder.buildVBox(CSG_SYLLABUS_SPLIT_PANE_OUTCOMES_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox syllabusSplitPaneOutcomesVBoxTopHBox = csgBuilder.buildHBox(CSG_SYLLABUS_SPLIT_PANE_OUTCOMES_VBOX_TOP_HBOX, syllabusSplitPaneOutcomesVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SYLLABUS_SPLIT_PANE_OUTCOMES_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON, syllabusSplitPaneOutcomesVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_SYLLABUS_SPLIT_PANE_OUTCOMES_VBOX_TOP_HBOX_LABEL, syllabusSplitPaneOutcomesVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYLLABUS_SPLIT_PANE_OUTCOMES_VBOX_TEXT_AREA, syllabusSplitPaneOutcomesVBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        VBox syllabusSplitPaneTextbooksVBox = csgBuilder.buildVBox(CSG_SYLLABUS_SPLIT_PANE_TEXTBOOKS_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox syllabusSplitPaneTextbooksVBoxTopHBox = csgBuilder.buildHBox(CSG_SYLLABUS_SPLIT_PANE_TEXTBOOKS_VBOX_TOP_HBOX, syllabusSplitPaneTextbooksVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SYLLABUS_SPLIT_PANE_TEXTBOOKS_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON, syllabusSplitPaneTextbooksVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_SYLLABUS_SPLIT_PANE_TEXTBOOKS_VBOX_TOP_HBOX_LABEL, syllabusSplitPaneTextbooksVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYLLABUS_SPLIT_PANE_TEXTBOOKS_VBOX_TEXT_AREA, syllabusSplitPaneTextbooksVBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        VBox syllabusSplitPaneGradedComponentsVBox = csgBuilder.buildVBox(CSG_SYLLABUS_SPLIT_PANE_GRADED_COMPONENTS_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox syllabusSplitPaneGradedComponentsVBoxTopHBox = csgBuilder.buildHBox(CSG_SYLLABUS_SPLIT_PANE_GRADED_COMPONENTS_VBOX_TOP_HBOX, syllabusSplitPaneGradedComponentsVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SYLLABUS_SPLIT_PANE_GRADED_COMPONENTS_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON, syllabusSplitPaneGradedComponentsVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_SYLLABUS_SPLIT_PANE_GRADED_COMPONENTS_VBOX_TOP_HBOX_LABEL, syllabusSplitPaneGradedComponentsVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYLLABUS_SPLIT_PANE_GRADED_COMPONENTS_VBOX_TEXT_AREA, syllabusSplitPaneGradedComponentsVBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        VBox syllabusSplitPaneGradingNotesVBox = csgBuilder.buildVBox(CSG_SYLLABUS_SPLIT_PANE_GRADING_NOTES_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox syllabusSplitPaneGradingNotesVBoxTopHBox = csgBuilder.buildHBox(CSG_SYLLABUS_SPLIT_PANE_GRADING_NOTES_VBOX_TOP_HBOX, syllabusSplitPaneGradingNotesVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SYLLABUS_SPLIT_PANE_GRADING_NOTES_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON, syllabusSplitPaneGradingNotesVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_SYLLABUS_SPLIT_PANE_GRADING_NOTES_VBOX_TOP_HBOX_LABEL, syllabusSplitPaneGradingNotesVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYLLABUS_SPLIT_PANE_GRADING_NOTES_VBOX_TEXT_AREA, syllabusSplitPaneGradingNotesVBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        VBox syllabusSplitPaneAcademicDishonestyVBox = csgBuilder.buildVBox(CSG_SYLLABUS_SPLIT_PANE_ACADEMIC_DISHONESTY_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox syllabusSplitPaneAcademicDishonestyVBoxTopHBox = csgBuilder.buildHBox(CSG_SYLLABUS_SPLIT_PANE_ACADEMIC_DISHONESTY_VBOX_TOP_HBOX, syllabusSplitPaneAcademicDishonestyVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SYLLABUS_SPLIT_PANE_ACADEMIC_DISHONESTY_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON, syllabusSplitPaneAcademicDishonestyVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_SYLLABUS_SPLIT_PANE_ACADEMIC_DISHONESTY_VBOX_TOP_HBOX_LABEL, syllabusSplitPaneAcademicDishonestyVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYLLABUS_SPLIT_PANE_ACADEMIC_DISHONESTY_VBOX_TEXT_AREA, syllabusSplitPaneAcademicDishonestyVBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        VBox syllabusSplitPaneSpecialAssistanceVBox = csgBuilder.buildVBox(CSG_SYLLABUS_SPLIT_PANE_SPECIAL_ASSISTANCE_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox syllabusSplitPaneSpecialAssistanceVBoxTopHBox = csgBuilder.buildHBox(CSG_SYLLABUS_SPLIT_PANE_SPECIAL_ASSISTANCE_VBOX_TOP_HBOX, syllabusSplitPaneSpecialAssistanceVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SYLLABUS_SPLIT_PANE_SPECIAL_ASSISTANCE_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON, syllabusSplitPaneSpecialAssistanceVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_SYLLABUS_SPLIT_PANE_SPECIAL_ASSISTANCE_VBOX_TOP_HBOX_LABEL, syllabusSplitPaneSpecialAssistanceVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildTextArea(CSG_SYLLABUS_SPLIT_PANE_SPECIAL_ASSISTANCE_VBOX_TEXT_AREA, syllabusSplitPaneSpecialAssistanceVBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        
        syllabusSplitPane.getItems().addAll(syllabusSplitPaneDescriptionVBox, syllabusSplitPaneTopicsVBox, syllabusSplitPanePrereqsVBox, syllabusSplitPaneOutcomesVBox,
                syllabusSplitPaneTextbooksVBox, syllabusSplitPaneGradedComponentsVBox, syllabusSplitPaneGradingNotesVBox, syllabusSplitPaneAcademicDishonestyVBox,
                syllabusSplitPaneSpecialAssistanceVBox);
        
        // MEETING TIMES TAB
        SplitPane meetingTimesSplitPane = csgBuilder.buildSplitPane(CSG_MEETING_TIMES_SPLIT_PANE, null, CLASS_CSG_PANE, ENABLED);
        
        meetingTimesSplitPane.setDividerPositions(.4);
        meetingTimesSplitPane.setOrientation(Orientation.VERTICAL);
        meetingTimesTab.setContent(new ScrollPane(meetingTimesSplitPane));
        
        VBox meetingTimesSplitPaneLecturesVBox = csgBuilder.buildVBox(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox meetingTimesSplitPaneLecturesVBoxTopHBox = csgBuilder.buildHBox(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_VBOX_TOP_HBOX, meetingTimesSplitPaneLecturesVBox,
             CLASS_CSG_PANE, ENABLED);
        
        csgBuilder.buildTextButton(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_VBOX_TOP_HBOX_ADD_BUTTON, meetingTimesSplitPaneLecturesVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildTextButton(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_VBOX_TOP_HBOX_REMOVE_BUTTON, meetingTimesSplitPaneLecturesVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_VBOX_TOP_HBOX_LABEL, meetingTimesSplitPaneLecturesVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        TableView<LectureDetails> meetingTimesSplitPaneLecturesTableView = csgBuilder.buildTableView(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_TABLE_VIEW,
                meetingTimesSplitPaneLecturesVBox, CLASS_CSG_TABLE_VIEW, ENABLED);
        meetingTimesSplitPaneLecturesTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn meetingTimesSplitPaneLecturesSectionColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_SECTION_TABLE_COLUMN,
                meetingTimesSplitPaneLecturesTableView, CLASS_CSG_COLUMN);
        TableColumn meetingTimesSplitPaneLecturesDaysColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_DAYS_TABLE_COLUMN,
                meetingTimesSplitPaneLecturesTableView, CLASS_CSG_COLUMN);
        TableColumn meetingTimesSplitPaneLecturesTimeColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_TIME_TABLE_COLUMN,
                meetingTimesSplitPaneLecturesTableView, CLASS_CSG_CENTERED_COLUMN);
        TableColumn meetingTimesSplitPaneLecturesRoomColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_ROOM_TABLE_COLUMN,
                meetingTimesSplitPaneLecturesTableView, CLASS_CSG_COLUMN);
        final CSGController controller = new CSGController((CSGApp) app);
        meetingTimesSplitPaneLecturesSectionColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneLecturesDaysColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneLecturesTimeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneLecturesRoomColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneLecturesSectionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        meetingTimesSplitPaneLecturesDaysColumn.setCellValueFactory(new PropertyValueFactory<String, String>("days"));
        meetingTimesSplitPaneLecturesTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("time"));
        meetingTimesSplitPaneLecturesRoomColumn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        meetingTimesSplitPaneLecturesSectionColumn.prefWidthProperty().bind(meetingTimesSplitPaneLecturesTableView.widthProperty().multiply(1.0 / 5.0));
        meetingTimesSplitPaneLecturesDaysColumn.prefWidthProperty().bind(meetingTimesSplitPaneLecturesTableView.widthProperty().multiply(2.0 / 5.0));
        meetingTimesSplitPaneLecturesTimeColumn.prefWidthProperty().bind(meetingTimesSplitPaneLecturesTableView.widthProperty().multiply(1.0 / 5.0));
        meetingTimesSplitPaneLecturesRoomColumn.prefWidthProperty().bind(meetingTimesSplitPaneLecturesTableView.widthProperty().multiply(1.0 / 5.0));
        
        VBox meetingTimesSplitPaneRecitationsVBox = csgBuilder.buildVBox(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox meetingTimesSplitPaneRecitationsVBoxTopHBox = csgBuilder.buildHBox(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_VBOX_TOP_HBOX, meetingTimesSplitPaneRecitationsVBox,
             CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_VBOX_TOP_HBOX_ADD_BUTTON, meetingTimesSplitPaneRecitationsVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildTextButton(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_VBOX_TOP_HBOX_REMOVE_BUTTON, meetingTimesSplitPaneRecitationsVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_VBOX_TOP_HBOX_LABEL, meetingTimesSplitPaneRecitationsVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        TableView<RecitationDetails> meetingTimesSplitPaneRecitationsTableView = csgBuilder.buildTableView(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_TABLE_VIEW,
                meetingTimesSplitPaneRecitationsVBox, CLASS_CSG_TABLE_VIEW, ENABLED);
        meetingTimesSplitPaneRecitationsTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn meetingTimesSplitPaneRecitationsSectionColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_SECTION_TABLE_COLUMN,
                meetingTimesSplitPaneRecitationsTableView, CLASS_CSG_COLUMN);
        TableColumn meetingTimesSplitPaneRecitationsDaysTimeColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_DAYS_TIME_TABLE_COLUMN,
                meetingTimesSplitPaneRecitationsTableView, CLASS_CSG_COLUMN);
        TableColumn meetingTimesSplitPaneRecitationsRoomColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_ROOM_TABLE_COLUMN,
                meetingTimesSplitPaneRecitationsTableView, CLASS_CSG_CENTERED_COLUMN);
        TableColumn meetingTimesSplitPaneRecitationsTAsColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_TAS_TABLE_COLUMN,
                meetingTimesSplitPaneRecitationsTableView, CLASS_CSG_COLUMN);
        meetingTimesSplitPaneRecitationsSectionColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneRecitationsDaysTimeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneRecitationsRoomColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneRecitationsTAsColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneRecitationsSectionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        meetingTimesSplitPaneRecitationsDaysTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("daysTime"));
        meetingTimesSplitPaneRecitationsRoomColumn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        meetingTimesSplitPaneRecitationsTAsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("tas"));
        meetingTimesSplitPaneRecitationsSectionColumn.prefWidthProperty().bind(meetingTimesSplitPaneRecitationsTableView.widthProperty().multiply(1.0 / 5.0));
        meetingTimesSplitPaneRecitationsDaysTimeColumn.prefWidthProperty().bind(meetingTimesSplitPaneRecitationsTableView.widthProperty().multiply(2.0 / 5.0));
        meetingTimesSplitPaneRecitationsRoomColumn.prefWidthProperty().bind(meetingTimesSplitPaneRecitationsTableView.widthProperty().multiply(1.0 / 5.0));
        meetingTimesSplitPaneRecitationsTAsColumn.prefWidthProperty().bind(meetingTimesSplitPaneRecitationsTableView.widthProperty().multiply(1.0 / 5.0));
        
        VBox meetingTimesSplitPaneLabsVBox = csgBuilder.buildVBox(CSG_MEETING_TIMES_SPLIT_PANE_LABS_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox meetingTimesSplitPaneLabsVBoxTopHBox = csgBuilder.buildHBox(CSG_MEETING_TIMES_SPLIT_PANE_LABS_VBOX_TOP_HBOX, meetingTimesSplitPaneLabsVBox,
             CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_MEETING_TIMES_SPLIT_PANE_LABS_VBOX_TOP_HBOX_ADD_BUTTON, meetingTimesSplitPaneLabsVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildTextButton(CSG_MEETING_TIMES_SPLIT_PANE_LABS_VBOX_TOP_HBOX_REMOVE_BUTTON, meetingTimesSplitPaneLabsVBoxTopHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_MEETING_TIMES_SPLIT_PANE_LABS_VBOX_TOP_HBOX_LABEL, meetingTimesSplitPaneLabsVBoxTopHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        TableView<LabDetails> meetingTimesSplitPaneLabsTableView = csgBuilder.buildTableView(CSG_MEETING_TIMES_SPLIT_PANE_LABS_TABLE_VIEW,
                meetingTimesSplitPaneLabsVBox, CLASS_CSG_TABLE_VIEW, ENABLED);
        meetingTimesSplitPaneLabsTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn meetingTimesSplitPaneLabsSectionColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_LABS_SECTION_TABLE_COLUMN,
                meetingTimesSplitPaneLabsTableView, CLASS_CSG_COLUMN);
        TableColumn meetingTimesSplitPaneLabsDaysTimeColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_LABS_DAYS_TIME_TABLE_COLUMN,
                meetingTimesSplitPaneLabsTableView, CLASS_CSG_COLUMN);
        TableColumn meetingTimesSplitPaneLabsRoomColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_LABS_ROOM_TABLE_COLUMN,
                meetingTimesSplitPaneLabsTableView, CLASS_CSG_CENTERED_COLUMN);
        TableColumn meetingTimesSplitPaneLabsTAsColumn = csgBuilder.buildTableColumn(CSG_MEETING_TIMES_SPLIT_PANE_LABS_TAS_TABLE_COLUMN,
                meetingTimesSplitPaneLabsTableView, CLASS_CSG_COLUMN);
        meetingTimesSplitPaneLabsSectionColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneLabsDaysTimeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneLabsRoomColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneLabsTAsColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        meetingTimesSplitPaneLabsSectionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        meetingTimesSplitPaneLabsDaysTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("daysTime"));
        meetingTimesSplitPaneLabsRoomColumn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        meetingTimesSplitPaneLabsTAsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("tas"));
        meetingTimesSplitPaneLabsSectionColumn.prefWidthProperty().bind(meetingTimesSplitPaneLabsTableView.widthProperty().multiply(1.0 / 5.0));
        meetingTimesSplitPaneLabsDaysTimeColumn.prefWidthProperty().bind(meetingTimesSplitPaneLabsTableView.widthProperty().multiply(2.0 / 5.0));
        meetingTimesSplitPaneLabsRoomColumn.prefWidthProperty().bind(meetingTimesSplitPaneLabsTableView.widthProperty().multiply(1.0 / 5.0));
        meetingTimesSplitPaneLabsTAsColumn.prefWidthProperty().bind(meetingTimesSplitPaneLabsTableView.widthProperty().multiply(1.0 / 5.0));
        
        meetingTimesSplitPane.getItems().addAll(meetingTimesSplitPaneLecturesVBox, meetingTimesSplitPaneRecitationsVBox, meetingTimesSplitPaneLabsVBox);
        
        // OFFICE HOURS TAB
        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane officeHoursSplitPane = csgBuilder.buildSplitPane(CSG_OFFICE_HOURS_SPLIT_PANE, null, CLASS_CSG_PANE, ENABLED);
        officeHoursSplitPane.setDividerPositions(.4);
        officeHoursSplitPane.setOrientation(Orientation.VERTICAL);
        officeHoursTab.setContent(new ScrollPane(officeHoursSplitPane));
        
        // INIT THE HEADER ON THE LEFT
        VBox leftPane = csgBuilder.buildVBox(CSG_OFFICE_HOURS_LEFT_PANE, null, CLASS_CSG_PANE, ENABLED);
        HBox tasHeaderBox = csgBuilder.buildHBox(CSG_OFFICE_HOURS_TAS_HEADER_PANE, leftPane, CLASS_CSG_BOX, ENABLED);
        csgBuilder.buildTextButton(CSG_OFFICE_HOURS_HEADER_REMOVE_BUTTON, tasHeaderBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSGPropertyType.CSG_OFFICE_HOURS_TAS_HEADER_LABEL, tasHeaderBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        HBox typeHeaderBox = csgBuilder.buildHBox(CSG_OFFICE_HOURS_GRAD_UNDERGRAD_TAS_PANE, tasHeaderBox, CLASS_CSG_RADIO_BOX, ENABLED);
        ToggleGroup tg = new ToggleGroup();
        csgBuilder.buildRadioButton(CSG_OFFICE_HOURS_ALL_RADIO_BUTTON, typeHeaderBox, CLASS_CSG_RADIO_BUTTON, ENABLED, tg, true);
        csgBuilder.buildRadioButton(CSG_OFFICE_HOURS_GRAD_RADIO_BUTTON, typeHeaderBox, CLASS_CSG_RADIO_BUTTON, ENABLED, tg, false);
        csgBuilder.buildRadioButton(CSG_OFFICE_HOURS_UNDERGRAD_RADIO_BUTTON, typeHeaderBox, CLASS_CSG_RADIO_BUTTON, ENABLED, tg, false);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        TableView<TeachingAssistantPrototype> taTable = csgBuilder.buildTableView(CSG_OFFICE_HOURS_TAS_TABLE_VIEW, leftPane, CLASS_CSG_TABLE_VIEW, ENABLED);
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn nameColumn = csgBuilder.buildTableColumn(CSG_OFFICE_HOURS_NAME_TABLE_COLUMN, taTable, CLASS_CSG_COLUMN);
        TableColumn emailColumn = csgBuilder.buildTableColumn(CSG_OFFICE_HOURS_EMAIL_TABLE_COLUMN, taTable, CLASS_CSG_COLUMN);
        TableColumn slotsColumn = csgBuilder.buildTableColumn(CSG_OFFICE_HOURS_SLOTS_TABLE_COLUMN, taTable, CLASS_CSG_CENTERED_COLUMN);
        TableColumn typeColumn = csgBuilder.buildTableColumn(CSG_OFFICE_HOURS_TYPE_TABLE_COLUMN, taTable, CLASS_CSG_COLUMN);
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<String, String>("email"));
        slotsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("slots"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(2.0 / 5.0));
        slotsColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));
        typeColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));

        // ADD BOX FOR ADDING A TA
        HBox taBox = csgBuilder.buildHBox(CSG_OFFICE_HOURS_ADD_TA_PANE, leftPane, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextField(CSG_OFFICE_HOURS_NAME_TEXT_FIELD, taBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        csgBuilder.buildTextField(CSG_OFFICE_HOURS_EMAIL_TEXT_FIELD, taBox, CLASS_CSG_TEXT_FIELD, ENABLED);
        csgBuilder.buildTextButton(CSG_OFFICE_HOURS_ADD_TA_BUTTON, taBox, CLASS_CSG_BUTTON, !ENABLED);

        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(taTable, Priority.ALWAYS);

        // INIT THE HEADER ON THE RIGHT
        VBox rightPane = csgBuilder.buildVBox(CSG_OFFICE_HOURS_RIGHT_PANE, null, CLASS_CSG_PANE, ENABLED);
        HBox officeHoursHeaderBox = csgBuilder.buildHBox(CSG_OFFICE_HOURS_HEADER_PANE, rightPane, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildLabel(CSG_OFFICE_HOURS_OFFICE_HOURS_HEADER_LABEL, officeHoursHeaderBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildLabel(CSG_OFFICE_HOURS_START_TIME_LABEL, officeHoursHeaderBox, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildComboBox(CSG_OFFICE_HOURS_START_TIME_COMBOBOX, CSG_OFFICE_HOURS_START_TIME_COMBOBOX_OPTIONS, CSG_OFFICE_HOURS_START_TIME_COMBOBOX_DEFAULT,
                officeHoursHeaderBox, CLASS_CSG_COMBOBOX, ENABLED);
        csgBuilder.buildLabel(CSG_OFFICE_HOURS_END_TIME_LABEL, officeHoursHeaderBox, CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildComboBox(CSG_OFFICE_HOURS_END_TIME_COMBOBOX, CSG_OFFICE_HOURS_END_TIME_COMBOBOX_OPTIONS, CSG_OFFICE_HOURS_END_TIME_COMBOBOX_DEFAULT,
                officeHoursHeaderBox, CLASS_CSG_COMBOBOX, ENABLED);

        // SETUP THE OFFICE HOURS TABLE
        TableView<TimeSlot> officeHoursTable = csgBuilder.buildTableView(CSG_OFFICE_HOURS_TABLE_VIEW, rightPane, CLASS_CSG_OFFICE_HOURS_TABLE_VIEW, ENABLED);
        setupCSGColumn(CSG_OFFICE_HOURS_START_TIME_TABLE_COLUMN, officeHoursTable, CLASS_CSG_OFFICE_HOURS_TIME_COLUMN, "startTime");
        setupCSGColumn(CSG_OFFICE_HOURS_END_TIME_TABLE_COLUMN, officeHoursTable, CLASS_CSG_OFFICE_HOURS_TIME_COLUMN, "endTime");
        setupCSGColumn(CSG_OFFICE_HOURS_MONDAY_TABLE_COLUMN, officeHoursTable, CLASS_CSG_OFFICE_HOURS_DAY_OF_WEEK_COLUMN, "monday");
        setupCSGColumn(CSG_OFFICE_HOURS_TUESDAY_TABLE_COLUMN, officeHoursTable, CLASS_CSG_OFFICE_HOURS_DAY_OF_WEEK_COLUMN, "tuesday");
        setupCSGColumn(CSG_OFFICE_HOURS_WEDNESDAY_TABLE_COLUMN, officeHoursTable, CLASS_CSG_OFFICE_HOURS_DAY_OF_WEEK_COLUMN, "wednesday");
        setupCSGColumn(CSG_OFFICE_HOURS_THURSDAY_TABLE_COLUMN, officeHoursTable, CLASS_CSG_OFFICE_HOURS_DAY_OF_WEEK_COLUMN, "thursday");
        setupCSGColumn(CSG_OFFICE_HOURS_FRIDAY_TABLE_COLUMN, officeHoursTable, CLASS_CSG_OFFICE_HOURS_DAY_OF_WEEK_COLUMN, "friday");

        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(officeHoursTable, Priority.ALWAYS);
        
        officeHoursSplitPane.getItems().addAll(leftPane, rightPane);
        
        // SCHEDULE TAB
        SplitPane scheduleSplitPane = csgBuilder.buildSplitPane(CSG_SCHEDULE_SPLIT_PANE, null, CLASS_CSG_PANE, ENABLED);
        scheduleSplitPane.setDividerPositions(.4);
        scheduleSplitPane.setOrientation(Orientation.VERTICAL);
        scheduleTab.setContent(new ScrollPane(scheduleSplitPane));
        
        HBox scheduleSplitPaneCalendarBoundsHBox = csgBuilder.buildHBox(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX, null, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildLabel(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_START_LABEL, scheduleSplitPaneCalendarBoundsHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        LocalDate endDate = LocalDate.now(), startDate = LocalDate.now();
        int d;
        for (d = 0; startDate.getDayOfWeek().getValue() != DayOfWeek.MONDAY.getValue(); d++)
            startDate = startDate.plusDays(1);
        endDate = endDate.plusDays(d + 4);
        csgBuilder.buildDatePicker(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_START_DATE_PICKER, scheduleSplitPaneCalendarBoundsHBox, CLASS_CSG_DATE_PICKER,
                ENABLED).setValue(startDate);
        csgBuilder.buildLabel(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_END_LABEL, scheduleSplitPaneCalendarBoundsHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        csgBuilder.buildDatePicker(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_END_DATE_PICKER, scheduleSplitPaneCalendarBoundsHBox, CLASS_CSG_DATE_PICKER,
                ENABLED).setValue(endDate);
        
        VBox scheduleSplitPaneScheduleVBox = csgBuilder.buildVBox(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX, null, CLASS_CSG_PANE, ENABLED);
        HBox scheduleSplitPaneScheduleVBoxHeaderHBox = csgBuilder.buildHBox(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_HEADER_HBOX, scheduleSplitPaneScheduleVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_REMOVE_BUTTON, scheduleSplitPaneScheduleVBoxHeaderHBox, CLASS_CSG_BUTTON, ENABLED);
        csgBuilder.buildLabel(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_LABEL, scheduleSplitPaneScheduleVBoxHeaderHBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        TableView<ScheduleItemPrototype> scheduleSplitPaneScheduleVBoxTableView = csgBuilder.buildTableView(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_TABLE_VIEW, scheduleSplitPaneScheduleVBox,
                CLASS_CSG_TABLE_VIEW, ENABLED);
        scheduleSplitPaneScheduleVBoxTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn scheduleSplitPaneScheduleVBoxTypeColumn = csgBuilder.buildTableColumn(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_TYPE_TABLE_COLUMN,
                scheduleSplitPaneScheduleVBoxTableView, CLASS_CSG_COLUMN);
        TableColumn scheduleSplitPaneScheduleVBoxDateColumn = csgBuilder.buildTableColumn(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_DATE_TABLE_COLUMN,
                scheduleSplitPaneScheduleVBoxTableView, CLASS_CSG_COLUMN);
        TableColumn scheduleSplitPaneScheduleVBoxTitleColumn = csgBuilder.buildTableColumn(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_TITLE_TABLE_COLUMN,
                scheduleSplitPaneScheduleVBoxTableView, CLASS_CSG_CENTERED_COLUMN);
        TableColumn scheduleSplitPaneScheduleVBoxTopicColumn = csgBuilder.buildTableColumn(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_TOPIC_TABLE_COLUMN,
                scheduleSplitPaneScheduleVBoxTableView, CLASS_CSG_COLUMN);
        scheduleSplitPaneScheduleVBoxTypeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        scheduleSplitPaneScheduleVBoxDateColumn.setCellValueFactory(new PropertyValueFactory<String, String>("date"));
        scheduleSplitPaneScheduleVBoxTitleColumn.setCellValueFactory(new PropertyValueFactory<String, String>("title"));
        scheduleSplitPaneScheduleVBoxTopicColumn.setCellValueFactory(new PropertyValueFactory<String, String>("topic"));
        scheduleSplitPaneScheduleVBoxTypeColumn.prefWidthProperty().bind(scheduleSplitPaneScheduleVBoxTableView.widthProperty().multiply(1.0 / 5.0));
        scheduleSplitPaneScheduleVBoxDateColumn.prefWidthProperty().bind(scheduleSplitPaneScheduleVBoxTableView.widthProperty().multiply(2.0 / 5.0));
        scheduleSplitPaneScheduleVBoxTitleColumn.prefWidthProperty().bind(scheduleSplitPaneScheduleVBoxTableView.widthProperty().multiply(1.0 / 5.0));
        scheduleSplitPaneScheduleVBoxTopicColumn.prefWidthProperty().bind(scheduleSplitPaneScheduleVBoxTableView.widthProperty().multiply(1.0 / 5.0));
        
        VBox scheduleSplitPaneEditVBox = csgBuilder.buildVBox(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX, null, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildLabel(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_LABEL, scheduleSplitPaneEditVBox, CLASS_CSG_HEADER_LABEL, ENABLED);
        GridPane scheduleSplitPaneEditVBoxGridPane = csgBuilder.buildGridPane(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE,
                scheduleSplitPaneEditVBox, CLASS_CSG_PANE, ENABLED);
        HBox scheduleSplitPaneEditVBoxGridPaneTypeHBox = csgBuilder.buildHBox(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TYPE_HBOX,
                scheduleSplitPaneEditVBoxGridPane, CLASS_CSG_PANE, ENABLED);
        scheduleSplitPaneEditVBoxGridPane.setConstraints(scheduleSplitPaneEditVBoxGridPaneTypeHBox, 0, 0);
        csgBuilder.buildLabel(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TYPE_LABEL, scheduleSplitPaneEditVBoxGridPaneTypeHBox,
                CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildComboBox(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TYPE_COMBOBOX,
                CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TYPE_COMBOBOX_OPTIONS,
                CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TYPE_COMBOBOX_DEFAULT, scheduleSplitPaneEditVBoxGridPaneTypeHBox, CLASS_CSG_COMBOBOX, ENABLED);
        HBox scheduleSplitPaneEditVBoxGridPaneDateHBox = csgBuilder.buildHBox(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_DATE_HBOX,
                scheduleSplitPaneEditVBoxGridPane, CLASS_CSG_PANE, ENABLED);
        scheduleSplitPaneEditVBoxGridPane.setConstraints(scheduleSplitPaneEditVBoxGridPaneDateHBox, 1, 0);
        csgBuilder.buildLabel(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_DATE_LABEL, scheduleSplitPaneEditVBoxGridPaneDateHBox,
                CLASS_CSG_LABEL, ENABLED);
        csgBuilder.buildDatePicker(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_DATE_PICKER, scheduleSplitPaneEditVBoxGridPaneDateHBox, CLASS_CSG_DATE_PICKER,
                ENABLED).setValue(startDate);
        scheduleSplitPaneEditVBoxGridPane.setConstraints(csgBuilder.buildTextField(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TITLE_TEXT_FIELD,
                scheduleSplitPaneEditVBoxGridPane, CLASS_CSG_TEXT_FIELD, ENABLED), 0, 1);
        scheduleSplitPaneEditVBoxGridPane.setConstraints(csgBuilder.buildTextField(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TOPIC_TEXT_FIELD,
                scheduleSplitPaneEditVBoxGridPane, CLASS_CSG_TEXT_FIELD, ENABLED), 1, 1);
        HBox scheduleSplitPaneEditVBoxBottomHBox = csgBuilder.buildHBox(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_BOTTOM_HBOX,
                scheduleSplitPaneEditVBox, CLASS_CSG_PANE, ENABLED);
        csgBuilder.buildTextButton(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_BOTTOM_HBOX_SET_BUTTON, scheduleSplitPaneEditVBoxBottomHBox,
                CLASS_CSG_TEXT_FIELD, ENABLED);
        csgBuilder.buildTextButton(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_BOTTOM_HBOX_CLEAR_BUTTON, scheduleSplitPaneEditVBoxBottomHBox,
                CLASS_CSG_TEXT_FIELD, ENABLED);
        
        scheduleSplitPane.getItems().addAll(scheduleSplitPaneCalendarBoundsHBox, scheduleSplitPaneScheduleVBox, scheduleSplitPaneEditVBox);
        
        // GENERIC
        
        workspace = new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(tabPane);
    }

    private void setupCSGColumn(Object columnId, TableView tableView, String styleClass, String columnDataProperty) {
        AppNodesBuilder builder = app.getGUIModule().getNodesBuilder();
        TableColumn<TeachingAssistantPrototype, String> column = builder.buildTableColumn(columnId, tableView, styleClass);
        column.setCellValueFactory(new PropertyValueFactory<TeachingAssistantPrototype, String>(columnDataProperty));
        column.prefWidthProperty().bind(tableView.widthProperty().multiply(1.0 / 7.0));
        column.setCellFactory(col -> {
            return new TableCell<TeachingAssistantPrototype, String>() {
                @Override
                protected void updateItem(String text, boolean empty) {
                    super.updateItem(text, empty);
                    if (text == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        // CHECK TO SEE IF text CONTAINS THE NAME OF
                        // THE CURRENTLY SELECTED TA
                        setText(text);
                        TableView<TeachingAssistantPrototype> tasTableView = (TableView) app.getGUIModule().getGUINode(CSG_OFFICE_HOURS_TAS_TABLE_VIEW);
                        TeachingAssistantPrototype selectedTA = tasTableView.getSelectionModel().getSelectedItem();
                        if (selectedTA == null) {
                            setStyle("");
                        } else if (text.contains(selectedTA.getName())) {
                            setStyle("-fx-background-color: yellow");
                        } else {
                            setStyle("");
                        }
                    }
                }
            };
        });
    }

    public void initControllers() {
        CSGController controller = new CSGController((CSGApp) app);
        AppGUIModule gui = app.getGUIModule();
        

        // FOOLPROOF DESIGN STUFF
        TextField nameTextField = ((TextField) gui.getGUINode(CSG_OFFICE_HOURS_NAME_TEXT_FIELD));
        TextField emailTextField = ((TextField) gui.getGUINode(CSG_OFFICE_HOURS_EMAIL_TEXT_FIELD));

        nameTextField.textProperty().addListener(e -> {
            controller.processTypeTA();
        });
        emailTextField.textProperty().addListener(e -> {
            controller.processTypeTA();
        });
        
        for (Object bn_pair[] : new Object[][]{ // show/hide text areas
                {CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_OFFICE_HOURS_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON,
                CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_OFFICE_HOURS_VBOX_TEXT_AREA
                },{CSG_SYLLABUS_SPLIT_PANE_DESCRIPTION_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON,
                CSG_SYLLABUS_SPLIT_PANE_DESCRIPTION_VBOX_TEXT_AREA
                },{CSG_SYLLABUS_SPLIT_PANE_TOPICS_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON,
                CSG_SYLLABUS_SPLIT_PANE_TOPICS_VBOX_TEXT_AREA
                },{CSG_SYLLABUS_SPLIT_PANE_PREREQS_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON,
                CSG_SYLLABUS_SPLIT_PANE_PREREQS_VBOX_TEXT_AREA
                },{CSG_SYLLABUS_SPLIT_PANE_OUTCOMES_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON,
                CSG_SYLLABUS_SPLIT_PANE_OUTCOMES_VBOX_TEXT_AREA
                },{CSG_SYLLABUS_SPLIT_PANE_TEXTBOOKS_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON,
                CSG_SYLLABUS_SPLIT_PANE_TEXTBOOKS_VBOX_TEXT_AREA
                },{CSG_SYLLABUS_SPLIT_PANE_GRADED_COMPONENTS_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON,
                CSG_SYLLABUS_SPLIT_PANE_GRADED_COMPONENTS_VBOX_TEXT_AREA
                },{CSG_SYLLABUS_SPLIT_PANE_GRADING_NOTES_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON,
                CSG_SYLLABUS_SPLIT_PANE_GRADING_NOTES_VBOX_TEXT_AREA
                },{CSG_SYLLABUS_SPLIT_PANE_ACADEMIC_DISHONESTY_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON,
                CSG_SYLLABUS_SPLIT_PANE_ACADEMIC_DISHONESTY_VBOX_TEXT_AREA
                },{CSG_SYLLABUS_SPLIT_PANE_SPECIAL_ASSISTANCE_VBOX_TOP_HBOX_SHOW_HIDE_BUTTON,
                CSG_SYLLABUS_SPLIT_PANE_SPECIAL_ASSISTANCE_VBOX_TEXT_AREA
                }}) {
            Button button = (Button) gui.getGUINode(bn_pair[0]);
            button.setText("-"); // initial value
            button.setOnAction(e -> {
                Node node = gui.getGUINode(bn_pair[1]);
                node.setManaged(!node.isManaged());
                node.setVisible(!node.isVisible());
                
                if (node.isVisible())
                    button.setText("-");
                else
                    button.setText("+");
            });
        }
        
        // page details action
        Node n;
        
        for (Object id : PageDetails.InputProperties) {
            n = gui.getGUINode(id);
            
            if (id.toString().endsWith("BUTTON"))
                ((Button) n).setOnAction(e -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Select Image");
                    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.ico", "*.tif"));
                    fileChooser.setInitialDirectory(new File(AppTemplate.PATH_IMAGES));
                    File f = fileChooser.showOpenDialog(this.app.getStage());
                    
                    if (f != null)
                        controller.processEditPageDetails(id, f.getPath());
                    else
                        controller.processEditPageDetails();
                });
            else if (id.toString().endsWith("CHECKBOX"))
                ((CheckBox) n).setOnAction(e -> {
                    controller.processEditPageDetails();
                });
            else if (id.toString().endsWith("COMBOBOX") || id.toString().endsWith("CHOOSER"))
                ((ComboBox) n).setOnAction(e -> {
                    controller.processEditPageDetails();
                });
            else if (id.toString().endsWith("TEXT_FIELD"))
                ((TextField) n).setOnKeyTyped(e -> {
                    controller.processEditPageDetails();
                });
        }
        
       for (Object id : InstructorDetails.InputProperties) {
            ((Node) gui.getGUINode(id)).setOnKeyTyped(e -> {
                controller.processEditInstructorDetails();
            });
        }
       
       for (Object id : SyllabusDetails.InputProperties) {
           ((Node) gui.getGUINode(id)).setOnKeyTyped(e -> {
                controller.processEditSyllabusDetails();
            });
       }
        
        // FIRE THE ADD EVENT ACTION
        nameTextField.setOnAction(e -> {
            controller.processAddTA();
        });
        emailTextField.setOnAction(e -> {
            controller.processAddTA();
        });
        ((Button) gui.getGUINode(CSG_OFFICE_HOURS_ADD_TA_BUTTON)).setOnAction(e -> {
            controller.processAddTA();
        });
        ((Button) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_VBOX_TOP_HBOX_ADD_BUTTON)).setOnAction(e -> {
            controller.processAddLecture();
        });
        ((Button) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_VBOX_TOP_HBOX_ADD_BUTTON)).setOnAction(e -> {
            controller.processAddRecitation();
        });
        ((Button) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LABS_VBOX_TOP_HBOX_ADD_BUTTON)).setOnAction(e -> {
            controller.processAddLab();
        });
        
        ((Button) gui.getGUINode(CSG_OFFICE_HOURS_HEADER_REMOVE_BUTTON)).setOnAction(e -> {
            controller.processRemoveTA();
        });
        ((Button) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_VBOX_TOP_HBOX_REMOVE_BUTTON)).setOnAction(e -> {
            controller.processRemoveLecture();
        });
        ((Button) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_VBOX_TOP_HBOX_REMOVE_BUTTON)).setOnAction(e -> {
            controller.processRemoveRecitation();
        });
        ((Button) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LABS_VBOX_TOP_HBOX_REMOVE_BUTTON)).setOnAction(e -> {
            controller.processRemoveLab();
        });

        TableView officeHoursTableView = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        officeHoursTableView.getSelectionModel().setCellSelectionEnabled(true);
        officeHoursTableView.setOnMouseClicked(e -> {
            controller.processToggleTA();
        });
        
        ((Button) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_BOTTOM_HBOX_SET_BUTTON)).setOnAction(e -> {
            if (((TableView) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_TABLE_VIEW)).getSelectionModel().getSelectedItem() == null)
                controller.processAddScheduleItem();
            else
                controller.processEditScheduleItem();
        });
        ((Button) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_REMOVE_BUTTON)).setOnAction(e -> {
            controller.processRemoveScheduleItem();
        });
        ((Button) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_BOTTOM_HBOX_CLEAR_BUTTON)).setOnAction(e -> {
            controller.processClearScheduleItemInputs();
        });
        ((TableView) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_TABLE_VIEW)).setOnMouseClicked(e -> {
            controller.processLoadScheduleItem();
        });
        // meeting times editing
        
        ((TableView) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_TABLE_VIEW)).setOnMouseClicked(e -> {
                controller.processToggleLectureEditing();
            
        });
        ((TableView) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_TABLE_VIEW)).setOnMouseClicked(e -> {
            controller.processToggleRecitationEditing();
        });
        ((TableView) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LABS_TABLE_VIEW)).setOnMouseClicked(e -> {
            controller.processToggleLabEditing();
        });
        try {
            for (TableColumn c : (java.util.List<TableColumn>) ((TableView) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_TABLE_VIEW)).getColumns())
                c.addEventHandler((javafx.event.EventType) c.getClass().getDeclaredField("EDIT_COMMIT_EVENT").get(c),
                        new javafx.event.EventHandler<TableColumn.CellEditEvent<TableCell, LectureDetails>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<TableCell, LectureDetails> event) {
                        controller.processLectureEdit();
                    }
                });
            for (TableColumn c : (java.util.List<TableColumn>) ((TableView) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_TABLE_VIEW)).getColumns())
                c.addEventHandler((javafx.event.EventType) c.getClass().getDeclaredField("EDIT_COMMIT_EVENT").get(c),
                        new javafx.event.EventHandler<TableColumn.CellEditEvent<TableCell, LectureDetails>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<TableCell, LectureDetails> event) {
                        controller.processRecitationEdit();
                    }
                });
            for (TableColumn c : (java.util.List<TableColumn>) ((TableView) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LABS_TABLE_VIEW)).getColumns())
                c.addEventHandler((javafx.event.EventType) c.getClass().getDeclaredField("EDIT_COMMIT_EVENT").get(c),
                        new javafx.event.EventHandler<TableColumn.CellEditEvent<TableCell, LectureDetails>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<TableCell, LectureDetails> event) {
                        controller.processLabEdit();
                    }
                });
        } catch (IllegalAccessException e) {
        } catch (NoSuchFieldException e) {
        }

        // DON'T LET ANYONE SORT THE TABLES
        TableView tasTableView = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TAS_TABLE_VIEW);
        for (int i = 0; i < officeHoursTableView.getColumns().size(); i++) {
            ((TableColumn) officeHoursTableView.getColumns().get(i)).setSortable(false);
        }
        for (int i = 0; i < tasTableView.getColumns().size(); i++) {
            ((TableColumn) tasTableView.getColumns().get(i)).setSortable(false);
        }

        tasTableView.setOnMouseClicked(e -> {
            app.getFoolproofModule().updateAll();
            if (e.getClickCount() == 2) {
                controller.processEditTA();
            }
            controller.processSelectTA();
        });
        
        ((ComboBox) gui.getGUINode(CSG_OFFICE_HOURS_START_TIME_COMBOBOX)).setOnAction(e -> {
            controller.processSetTimeRange();
        });
        ((ComboBox) gui.getGUINode(CSG_OFFICE_HOURS_END_TIME_COMBOBOX)).setOnAction(e -> {
            controller.processSetTimeRange();
        });
        
        for (Object id : new Object[]{CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_START_DATE_PICKER,
                CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_END_DATE_PICKER})
            ((DatePicker) gui.getGUINode(id)).setOnAction(e -> {
                controller.processEditStartEndDates();
            });
        ((Button) gui.getGUINode(CSG_OFFICE_HOURS_HEADER_REMOVE_BUTTON)).setOnAction(e -> {
            controller.processRemoveTA();
        });
        RadioButton allRadio = (RadioButton) gui.getGUINode(CSG_OFFICE_HOURS_ALL_RADIO_BUTTON);
        allRadio.setOnAction(e -> {
            controller.processSelectAllTAs();
        });
        RadioButton gradRadio = (RadioButton) gui.getGUINode(CSG_OFFICE_HOURS_GRAD_RADIO_BUTTON);
        gradRadio.setOnAction(e -> {
            controller.processSelectGradTAs();
        });
        RadioButton undergradRadio = (RadioButton) gui.getGUINode(CSG_OFFICE_HOURS_UNDERGRAD_RADIO_BUTTON);
        undergradRadio.setOnAction(e -> {
            controller.processSelectUndergradTAs();
        });
    }

    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(CSG_FOOLPROOF_SETTINGS,
                new CSGFoolproofDesign((CSGApp) app));
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        // WE AREN'T USING THIS FOR THIS APPLICATION
    }

    @Override
    public void showNewDialog() {
        // WE AREN'T USING THIS FOR THIS APPLICATION
    }
}
