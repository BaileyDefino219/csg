package csg.workspace.controllers;

import djf.modules.AppGUIModule;
import djf.ui.dialogs.AppDialogsFacade;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javafx.collections.ObservableList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import csg.CSGApp;
import javafx.beans.property.SimpleStringProperty;
import static csg.CSGPropertyType.*;
import csg.data.*;
import csg.data.LectureDetails;
import csg.data.PageDetails;
import csg.data.TAType;
import csg.data.TeachingAssistantPrototype;
import csg.data.TimeSlot;
import csg.data.TimeSlot.DayOfWeek;
import csg.transactions.*;
import csg.workspace.dialogs.TADialog;
import java.time.LocalDate;
import java.util.ArrayList;
import properties_manager.PropertiesManager;
/**
 *
 * @author McKillaGorilla
 */
public class CSGController {

    CSGApp app;

    public CSGController(CSGApp initApp) {
        app = initApp;
    }

    public void processAddTA() {
        AppGUIModule gui = app.getGUIModule();
        TextField nameTF = (TextField) gui.getGUINode(CSG_OFFICE_HOURS_NAME_TEXT_FIELD);
        String name = nameTF.getText();
        TextField emailTF = (TextField) gui.getGUINode(CSG_OFFICE_HOURS_EMAIL_TEXT_FIELD);
        String email = emailTF.getText();
        CSGData data = (CSGData) app.getDataComponent();
        TAType type = data.getSelectedType();
        if (data.isLegalNewTA(name, email)) {
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name.trim(), email.trim(), type);
            AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
            app.processTransaction(addTATransaction);

            // NOW CLEAR THE TEXT FIELDS
            nameTF.setText("");
            emailTF.setText("");
            nameTF.requestFocus();
        }
        app.getFoolproofModule().updateControls(CSG_FOOLPROOF_SETTINGS);
    }

    public void processVerifyTA() {
        
    }

    public void processToggleTA() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TablePosition> selectedCells = officeHoursTableView.getSelectionModel().getSelectedCells();
        if (selectedCells.size() > 0) {
            TablePosition cell = selectedCells.get(0);
            int cellColumnNumber = cell.getColumn();
            CSGData data = (CSGData)app.getDataComponent();
            if (data.isDayOfWeekColumn(cellColumnNumber)) {
                DayOfWeek dow = data.getColumnDayOfWeek(cellColumnNumber);
                TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(CSG_OFFICE_HOURS_TAS_TABLE_VIEW);
                TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
                if (ta != null) {
                    TimeSlot timeSlot = officeHoursTableView.getSelectionModel().getSelectedItem();
                    ToggleTA_Transaction transaction = new ToggleTA_Transaction(data, timeSlot, dow, ta);
                    app.processTransaction(transaction);
                }
                else {
                    Stage window = app.getGUIModule().getWindow();
                    AppDialogsFacade.showMessageDialog(window, CSG_NO_TA_SELECTED_TITLE, CSG_NO_TA_SELECTED_CONTENT);
                }
            }
            int row = cell.getRow();
            cell.getTableView().refresh();
        }
    }

    public void processTypeTA() {
        app.getFoolproofModule().updateControls(CSG_FOOLPROOF_SETTINGS);
    }

    public void processEditTA() {
        CSGData data = (CSGData)app.getDataComponent();
        if (data.isTASelected()) {
            TeachingAssistantPrototype taToEdit = data.getSelectedTA();
            TADialog taDialog = (TADialog)app.getGUIModule().getDialog(CSG_TA_EDIT_DIALOG);
            taDialog.showEditDialog(taToEdit);
            TeachingAssistantPrototype editTA = taDialog.getEditTA();
            if (editTA != null) {
                EditTA_Transaction transaction = new EditTA_Transaction(taToEdit, editTA.getName(), editTA.getEmail(), editTA.getType());
                app.processTransaction(transaction);
            }
        }
    }

    public void processSelectAllTAs() {
        CSGData data = (CSGData)app.getDataComponent();
        data.selectTAs(TAType.All);
    }

    public void processSelectGradTAs() {
        CSGData data = (CSGData)app.getDataComponent();
        data.selectTAs(TAType.Graduate);
    }

    public void processSelectUndergradTAs() {
        CSGData data = (CSGData)app.getDataComponent();
        data.selectTAs(TAType.Undergraduate);
    }

    public void processSelectTA() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        officeHoursTableView.refresh();
    }
    
    /* AFTER A FEW USES, THIS INCORRECTLY PARSES THE DATES */
    public void processSetTimeRange() {
        AppGUIModule gui = app.getGUIModule();
        ComboBox startTime = (ComboBox) gui.getGUINode(CSG_OFFICE_HOURS_START_TIME_COMBOBOX);
        ComboBox endTime = (ComboBox) gui.getGUINode(CSG_OFFICE_HOURS_END_TIME_COMBOBOX);
        long si = 0L, ei = 0L;
        DateFormat fmt = new SimpleDateFormat("hh:mm aa");
        try { // convert to long format
            si = fmt.parse((String) startTime.getValue()).toInstant().getEpochSecond();
            ei = fmt.parse((String) endTime.getValue()).toInstant().getEpochSecond();
        } catch (ParseException e) {
            
        } catch (UnsupportedOperationException e) {
            
        }
        if (si < ei) // g2g
            ((CSGData) app.getDataComponent()).setTimeRange(si, ei);
    }
    
    public void processEditPageDetails() {
        processEditPageDetails(null, null);
    }
    
    public void processEditPageDetails(Object chooserButtonID, String imagePath) {
        AppGUIModule gui = app.getGUIModule();
        
        for (Object id : PageDetails.InputProperties) { // make sure a checkbox is selected
            if (id.toString().endsWith("CHECKBOX") && ((CheckBox) gui.getGUINode(id)).isSelected()) {
                app.processTransaction(new EditPageDetails_Transaction((CSGData) app.getDataComponent(), chooserButtonID, imagePath));
                break;
            }
        }
    }
    
    public void processEditInstructorDetails() {
        app.processTransaction(new EditInstructorDetails_Transaction((CSGData) app.getDataComponent()));
    }
    
    public void processEditSyllabusDetails() {
        app.processTransaction(new EditSyllabusDetails_Transaction((CSGData) app.getDataComponent()));
    }
    
    public void processRemoveTA() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(CSG_OFFICE_HOURS_TAS_TABLE_VIEW);
                TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
                if (ta != null) {
                    app.processTransaction(new RemoveTA_Transaction((CSGData) app.getDataComponent(), ta));
                }
                else {
                    Stage window = app.getGUIModule().getWindow();
                    AppDialogsFacade.showMessageDialog(window, CSG_NO_TA_SELECTED_TITLE, CSG_NO_TA_SELECTED_CONTENT);
                }
    }
    
    public void processAddLecture() {
        app.processTransaction(new AddLecture_Transaction((CSGData) app.getDataComponent(), new LectureDetails("?", "?", "?", "?")));
    }
    
    public void processRemoveLecture() {
        LectureDetails lecture = (LectureDetails) ((TableView) app.getGUIModule().getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_TABLE_VIEW
            )).getSelectionModel().getSelectedItem();
        if (lecture == null)
            return;
        app.processTransaction(new RemoveLecture_Transaction((CSGData) app.getDataComponent(), lecture));
    }
    
    public void processAddRecitation() {
        app.processTransaction(new AddRecitation_Transaction((CSGData) app.getDataComponent(), new RecitationDetails("?", "?", "?", "?")));
    }
    
    public void processRemoveRecitation() {
        RecitationDetails o = (RecitationDetails) ((TableView) app.getGUIModule().getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_TABLE_VIEW
            )).getSelectionModel().getSelectedItem();
        if (o == null)
            return;
        app.processTransaction(new RemoveRecitation_Transaction((CSGData) app.getDataComponent(), o));
    }
    
    public void processAddLab() {
        app.processTransaction(new AddLab_Transaction((CSGData) app.getDataComponent(), new LabDetails("?", "?", "?", "?")));
    }
    
    public void processRemoveLab() {
        LabDetails o = (LabDetails) ((TableView) app.getGUIModule().getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LABS_TABLE_VIEW
            )).getSelectionModel().getSelectedItem();
        if (o == null)
            return;
        app.processTransaction(new RemoveLab_Transaction((CSGData) app.getDataComponent(), o));
    }
    
    public void processToggleLectureEditing() {
        TableView table = (TableView) app.getGUIModule().getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_TABLE_VIEW);
        table.setEditable(true);
        TablePosition p = table.getFocusModel().getFocusedCell();
        
        if (p == null)
            return;
        try {
            p.getTableColumn().setEditable(true);
            table.edit(p.getRow(), p.getTableColumn());
        } catch (NullPointerException e) {
            
        }
    }
    public void processLectureEdit() {
        TableView table = (TableView) app.getGUIModule().getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_TABLE_VIEW);
        if (table.getSelectionModel().getSelectedItem() == null)
            return;
        app.processTransaction(new EditLecture_Transaction((CSGData) app.getDataComponent(),
                    (LectureDetails) table.getSelectionModel().getSelectedItem())); // saves a copy of the old stuff
    }
    public void processToggleRecitationEditing() {
        TableView table = (TableView) app.getGUIModule().getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_TABLE_VIEW);
        table.setEditable(true);
        TablePosition p = table.getFocusModel().getFocusedCell();
        
        if (p == null)
            return;
        try {
            p.getTableColumn().setEditable(true);
            table.edit(p.getRow(), p.getTableColumn());
        } catch (NullPointerException e) {
            
        }
    }
    public void processRecitationEdit() {
        TableView table = (TableView) app.getGUIModule().getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_TABLE_VIEW);
        if (table.getSelectionModel().getSelectedItem() == null)
            return;
        app.processTransaction(new EditRecitation_Transaction((CSGData) app.getDataComponent(),
                    (RecitationDetails) table.getSelectionModel().getSelectedItem())); // saves a copy of the old stuff
    }
    public void processToggleLabEditing() {
        TableView table = (TableView) app.getGUIModule().getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LABS_TABLE_VIEW);
        table.setEditable(true);
        TablePosition p = table.getFocusModel().getFocusedCell();
        
        if (p == null)
            return;
        try {
            p.getTableColumn().setEditable(true);
            table.edit(p.getRow(), p.getTableColumn());
        } catch (NullPointerException e) {
            
        }
    }
    public void processLabEdit() {
        TableView table = (TableView) app.getGUIModule().getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LABS_TABLE_VIEW);
        if (table.getSelectionModel().getSelectedItem() == null)
            return;
        app.processTransaction(new EditLab_Transaction((CSGData) app.getDataComponent(),
                    (LabDetails) table.getSelectionModel().getSelectedItem())); // saves a copy of the old stuff
    }
    public void processAddScheduleItem() {
        AppGUIModule gui = app.getGUIModule();
        DatePicker dateP = (DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_DATE_PICKER);
        TextField titleTF = (TextField) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TITLE_TEXT_FIELD);
        TextField topicTF = (TextField) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TOPIC_TEXT_FIELD);
        ComboBox typeCB = (ComboBox) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TYPE_COMBOBOX);
        LocalDate date = dateP.getValue();
        
        if (typeCB.getSelectionModel().getSelectedItem() == null)
            return;
        if (date.isBefore(((DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_START_DATE_PICKER)).getValue())
                || date.isAfter(((DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_END_DATE_PICKER)).getValue())) { // not in range
            djf.ui.dialogs.AppDialogsFacade.showMessageDialog(app.getStage(), "Date!", "Schedule item date not within range.");
            return;
        }
        app.processTransaction(new AddScheduleItem_Transaction((CSGData) app.getDataComponent(), new ScheduleItemPrototype(date.toString(), titleTF.getText(),
            topicTF.getText(), (String) typeCB.getSelectionModel().getSelectedItem())));
        processClearScheduleItemInputs();
    }
    public void processRemoveScheduleItem() {
        AppGUIModule gui = app.getGUIModule();
        ScheduleItemPrototype o = (ScheduleItemPrototype) ((TableView) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_TABLE_VIEW
            )).getSelectionModel().getSelectedItem();
        if (o != null)
            app.processTransaction(new RemoveScheduleItem_Transaction((CSGData) app.getDataComponent(), o));
    }
    public void processClearScheduleItemInputs() {
        AppGUIModule gui = app.getGUIModule();
        ((TableView) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_TABLE_VIEW)).getSelectionModel().clearSelection();
        for (Object id : new Object[]{
            CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TITLE_TEXT_FIELD,
            CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TOPIC_TEXT_FIELD})
            ((TextField) gui.getGUINode(id)).setText("");
        ((DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_DATE_PICKER)).setValue(
                ((DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_START_DATE_PICKER)).getValue());
        ((ComboBox) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TYPE_COMBOBOX)).setValue(
                PropertiesManager.getPropertiesManager().getProperty(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TYPE_COMBOBOX_DEFAULT));
    }
    public void processLoadScheduleItem() {
        AppGUIModule gui = app.getGUIModule();
        ScheduleItemPrototype o = (ScheduleItemPrototype) ((TableView) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_TABLE_VIEW)).getSelectionModel().getSelectedItem();
        if (o == null) {
            processClearScheduleItemInputs();
            return;
        }
        ((TextField) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TITLE_TEXT_FIELD)).setText(o.title.get());
        ((TextField) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TOPIC_TEXT_FIELD)).setText(o.topic.get());
        ((DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_DATE_PICKER)).setValue(LocalDate.parse(o.date.get()));
        ((ComboBox) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TYPE_COMBOBOX)).setValue(o.type.get());
    }
    public void processEditScheduleItem() {
        AppGUIModule gui = app.getGUIModule();
        ScheduleItemPrototype o = (ScheduleItemPrototype) ((TableView) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_TABLE_VIEW)).getSelectionModel().getSelectedItem();
        if (o == null) {
            processClearScheduleItemInputs();
            return;
        }
        DatePicker dateP = (DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_DATE_PICKER);
        TextField titleTF = (TextField) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TITLE_TEXT_FIELD);
        TextField topicTF = (TextField) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TOPIC_TEXT_FIELD);
        ComboBox typeCB = (ComboBox) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_EDIT_VBOX_GRID_PANE_TYPE_COMBOBOX);
        LocalDate date = dateP.getValue();
        if (typeCB.getSelectionModel().getSelectedItem() == null)
            return;
        if (date.isBefore(((DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_START_DATE_PICKER)).getValue())
                || date.isAfter(((DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_END_DATE_PICKER)).getValue())) { // not in range
            djf.ui.dialogs.AppDialogsFacade.showMessageDialog(app.getStage(), "Date!", "Schedule item date not within range.");
            return;
        }
        CSGData data = (CSGData) app.getDataComponent();
        app.processTransaction(new EditScheduleItem_Transaction(data, o, new ScheduleItemPrototype(date.toString(), titleTF.getText(),
            topicTF.getText(), (String) typeCB.getSelectionModel().getSelectedItem())));
        processClearScheduleItemInputs();
    }
    public void processEditStartEndDates() { // disregard bs
        AppGUIModule gui = app.getGUIModule();
        LocalDate start = ((DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_START_DATE_PICKER)).getValue();
        LocalDate stop = ((DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_END_DATE_PICKER)).getValue();
        
        if (start.isAfter(stop) || start.isEqual(stop) || start.getDayOfWeek() != java.time.DayOfWeek.MONDAY
                    || stop.getDayOfWeek() != java.time.DayOfWeek.FRIDAY) {
            djf.ui.dialogs.AppDialogsFacade.showMessageDialog(app.getStage(), "Date!", "Bad date(s).");
            ((CSGData) app.getDataComponent()).updateStartEndDates();
            return;
        }
        app.processTransaction(new EditStartEndDates_Transaction((CSGData) app.getDataComponent(), start.toString(), stop.toString()));
    }
}
