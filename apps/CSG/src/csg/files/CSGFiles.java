package csg.files;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import csg.CSGApp;
import csg.data.*;
import csg.data.TimeSlot.DayOfWeek;
import static djf.AppPropertyType.APP_EXPORT_PAGE;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import properties_manager.PropertiesManager;

/**
 * This class serves as the file component for the TA
 * manager app. It provides all saving and loading 
 * services for the application.
 * 
 * @author Richard McKenna
 */
public class CSGFiles implements AppFileComponent {
    // THIS IS THE APP ITSELF
    CSGApp app;
    
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_GRAD_TAS = "grad_tas";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_NAME = "name";
    static final String JSON_EMAIL = "email";
    static final String JSON_TYPE = "type";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_START_TIME = "time";
    static final String JSON_DAY_OF_WEEK = "day";
    static final String JSON_MONDAY = "monday";
    static final String JSON_TUESDAY = "tuesday";
    static final String JSON_WEDNESDAY = "wednesday";
    static final String JSON_THURSDAY = "thursday";
    static final String JSON_FRIDAY = "friday";
    
    public static final String STATE = "./app_data/.state";

    public CSGFiles(CSGApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        data.reset();
	_pdserializeOfficeHoursData((CSGData) data, filePath);
        _pdserializePageData((CSGData) data, filePath);
        _pdserializeScheduleData((CSGData) data, filePath);
        _pdserializeSyllabusData((CSGData) data, filePath);
    }
    
    public void loadTAs(CSGData data, JsonObject json, String tas) {
        JsonArray jsonTAArray = json.getJsonArray(tas);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            TAType type = TAType.valueOf(jsonTA.getString(JSON_TYPE));
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name, email, type);
            data.addTA(ta);
        }     
    }
      
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    public JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	_pserializeOfficeHoursData(filePath, (CSGData) data);
        _pserializePageData(filePath, (CSGData) data);
        _pserializeScheduleData(filePath, (CSGData) data);
        _pserializeSyllabusData(filePath, (CSGData) data);
    }
    
    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        loadData(data, "./work/" + filePath);
        PageDetails page = ((CSGData) data).getPageDetails();
        File dest = new File(page.getExportDir());
        if (!dest.exists())
            dest.mkdirs();
        java.nio.file.Files.walk(dest.toPath())
            .sorted(java.util.Comparator.reverseOrder())
            .map(java.nio.file.Path::toFile)
            .forEach(File::delete);
        csg.files.Copy.copyTree(new File("./app_data/.site").toPath(), dest.toPath());
        saveData(data, page.getExportDir() + "/js/");
        PropertiesManager.getPropertiesManager().addProperty(APP_EXPORT_PAGE, page.getExportDir() + "/index.html");
    }
    
    
    
    
    
    
    
    
    public static final String OFFICE_HOURS_DATA_JSON = "officeHoursData.json";
    public static final String PAGE_DATA_JSON = "pageData.json";
    public static final String SCHEDULE_DATA_JSON = "scheduleData.json";
    public static final String SYLLABUS_DATA_JSON = "syllabusData.json";
    
    
    public static final String SUBJECT = "subject";
    public static final String NUMBER  = "number";
    public static final String YEAR = "year";
    public static final String SEMESTER = "semester";
    public static final String TITLE = "title";
    public static final String HREF = "href";
    public static final String SRC = "src";
    public static final String LOGOS = "logos";
    public static final String FAVICON = "favicon";
    public static final String NAVBAR = "navbar";
    public static final String LEFT_FOOTER = "bottom_left";
    public static final String RIGHT_FOOTER = "bottom_right";
    public static final String INSTRUCTOR = "instructor";
    public static final String NAME = "name";
    public static final String LINK = "link";
    public static final String EMAIL = "email";
    public static final String ROOM = "room";
    public static final String HOURS = "hours";
    public static final String PAGES = "pages";
    public static final String DESCRIPTION = "description";
    public static final String TOPICS = "topics";
    public static final String TEXTBOOKS = "textbooks";
    public static final String GRADED_COMPONENTS = "gradedComponents";
    public static final String PREREQUISITES = "prerequisites";
    public static final String OUTCOMES = "outcomes";
    public static final String GRADING_NOTE = "gradingNote";
    public static final String ACADEMIC_DISHONESTY = "academicDishonesty";
    public static final String SPECIAL_ASSISTANCE = "specialAssistance";
    
    public String _writeString(javax.json.JsonStructure s) {
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.write(s);
	jsonWriter.close();
        return sw.toString();
    }
    public void _pdserializeOfficeHoursData(CSGData dest, String dir) throws IOException {
       // CLEAR THE OLD DATA OUT
	CSGData dataManager = (CSGData)dest;

	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(dir + "/" + OFFICE_HOURS_DATA_JSON);

	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);
        
        JsonObject instructor = json.getJsonObject(INSTRUCTOR);
        InstructorDetails instructorDest = dest.getInstructorDetails();
        instructorDest.setName(instructor.getString(NAME));
        instructorDest.setEmail(instructor.getString(EMAIL));
        instructorDest.setRoom(instructor.getString(ROOM));
        instructorDest.setWebsite(instructor.getString(LINK));
        instructorDest.setOfficeHours(_writeString(instructor.getJsonArray(HOURS)));
        
        // LOAD ALL THE GRAD TAs
        loadTAs(dataManager, json, JSON_GRAD_TAS);
        loadTAs(dataManager, json, JSON_UNDERGRAD_TAS);

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonCSGArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonCSGArray.size(); i++) {
            JsonObject jsonCSG = jsonCSGArray.getJsonObject(i);
            String startTime = jsonCSG.getString(JSON_START_TIME);
            DayOfWeek dow = DayOfWeek.valueOf(jsonCSG.getString(JSON_DAY_OF_WEEK));
            String name = jsonCSG.getString(JSON_NAME);
            TeachingAssistantPrototype ta = dataManager.getTAWithName(name);
            TimeSlot timeSlot = dataManager.getTimeSlot(startTime);
            timeSlot.toggleTA(dow, ta);
        }
    }
    
    public void _pdserializePageData(CSGData dest, String dir) throws IOException  {
         /*
        {
    "subject": SUBJECT,
    "number": NUMBER,
    "semester": SEMESTER,
    "year": YEAR,
    "title": TITLE,
    "logos": {
        "favicon": {
            "href": FAVICON
        },
        "navbar": {
            "href": NAVBAR,
            "src": NAVBAR
        },
        "bottom_left": {
            "href": BOTTOM-LEFT,
            "src": BOTTOM-LEFT
        },
        "bottom_right": {
            "href": BOTTOM-RIGHT,
            "src": BOTTOM-RIGHT
        }
    },
    "instructor": {
        		"name":    NAME,
        		"link":     WEBSITE,
        		"email":    EMAIL,
        		"room":     ROOM,
                                                                    "photo":    PATH,
        		"hours":    HOURS
	},
    "pages": [ {
		“name”: NAME,
		“link”: LINK
            }, ...]
	}
        */
        // CLEAR THE OLD DATA OUT
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(dir + "/" + PAGE_DATA_JSON);
        PageDetails page = ((CSGData) dest).getPageDetails();
        
        page.setSubject(json.getString(SUBJECT));
        page.setNumber(json.getInt(NUMBER));
        page.setSemester(json.getString(SEMESTER));
        page.setYear(json.getInt(YEAR));
        page.setTitle(json.getString(TITLE));
        page.setExportDir("./export/" + page.getSubject() + "_" + page.getNumber() + "_" + page.getSemester() + "_" + page.getYear() + "/");
        
        JsonObject logos = json.getJsonObject(LOGOS);
        
        page.setFaviconPath(logos.getJsonObject(FAVICON).getString(HREF));
        page.setNavbarPath(logos.getJsonObject(NAVBAR).getString(HREF));
        page.setLeftFooterPath(logos.getJsonObject(LEFT_FOOTER).getString(HREF));
        page.setRightFooterPath(logos.getJsonObject(RIGHT_FOOTER).getString(HREF));
        
        InstructorDetails instructorDetails = ((CSGData) dest).getInstructorDetails();
        JsonObject instructor = json.getJsonObject(INSTRUCTOR);
        
        instructorDetails.setName(instructor.getString(NAME));
        instructorDetails.setEmail(instructor.getString(EMAIL));
        instructorDetails.setRoom(instructor.getString(ROOM));
        instructorDetails.setWebsite(instructor.getString(LINK));
        instructorDetails.setOfficeHours(_writeString(instructor.getJsonArray(HOURS)));
        
        JsonArray pages = json.getJsonArray(PAGES);
        for (int i = 0; i < pages.size(); i++) {
            JsonObject p = pages.getJsonObject(i);
            String n = p.getString(NAME);
            
            if (n.equals("Home"))
                page.setIncludeHomePage(true);
            else if (n.equals("HWs"))
                page.setIncludeHWsPage(true);
            else if (n.equals("Schedule"))
                page.setIncludeSchedulePage(true);
            else if (n.equals("Syllabus"))
                page.setIncludeSyllabusPage(true);
        }
        
         dest.updateInstructorDetails();
         dest.updatePageDetails();
    }
    private static String DATE = "date";
    public void _pdserializeScheduleData(CSGData dest, String dir) throws IOException  {
         /*
        {
            "startingMondayMonth":  INT,
            "startingMondayDay":    INT,
            "endingFridayMonth":    INT,
            "endingFridayDay":      INT,

            "holidays": [
                { "month": "12", "day": "21", "title": "", "link": "" }
            ],
        
            "labs":[
                        { "month": INT,     "day": INT,     "title": STRING,      "topic": STRING,    "link": STRING },
             ],

            "lectures":[
                { "month": INT,     "day": INT,     "title": STRING,      "topic": STRING,    "link": STRING}
            ],

            "references": [
                { "month": INT, "day": INT, "title": STRING, "topic": STRING, "link": STRING }
            ],

            "recitations":[
                        { "month": INT,     "day": INT,     "title": STRING,      "topic": STRING,    "link": STRING },
             ],
            "hws": [
                { "month": INT, "day": INT,    "title": STRING,    "topic": STRING,      "link": STRING,     "time": STRING,  "criteria": STRING     }
            ]
        }
        */
        // CLEAR THE OLD DATA OUT
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(dir + "/" + SCHEDULE_DATA_JSON);
        CSGData data = (CSGData) dest;
        List<ScheduleItemPrototype> schedule = data.getScheduleItems();
        List<LectureDetails> lectures = data.getLectures();
        List<RecitationDetails> recitations = data.getRecitations();
        List<LabDetails> labs = data.getLabs();
        schedule.clear();
        lectures.clear();
        recitations.clear();
        labs.clear();
        
        data.startingMonday = data.getPageDetails().getYear() + "-" + json.getString(STARTING_MONDAY_MONTH) + "-" + json.getString(STARTING_MONDAY_DAY);
        data.endingFriday = data.getPageDetails().getYear() + "-" + json.getString(ENDING_FRIDAY_MONTH) + "-" + json.getString(ENDING_FRIDAY_DAY);
        
        for (String key : new String[]{HOLIDAYS, HWS, REFERENCES}) {
            JsonArray arr = json.getJsonArray(key);
            String _realkey = "Unknown";
            switch (key) {
                case "hws":
                    _realkey = "HW";
                    break;
                case "holidays":
                    _realkey = "Holiday";
                    break;
                case "labs":
                    _realkey = "Lab";
                    break;
                case "lectures":
                    _realkey = "Lecture";
                    break;
                case "recitations":
                    _realkey = "Recitation";
                    break;
                case "references":
                    _realkey = "Reference";
            }
            for (int i = 0; i < arr.size(); i++) {
                JsonObject o = arr.getJsonObject(i);
                schedule.add(new ScheduleItemPrototype(data.getPageDetails().getYear() + "-" + o.getString(MONTH) + "-" + o.getString(DAY), o.getString(TITLE),
                        o.getString(TOPIC), _realkey));
            }
        }
    }
    
    public void _pdserializeSyllabusData(CSGData dest, String dir) throws IOException  {
        /*
        {
    "description": STRING,
    "topics": [STRING,]*,
    "prerequisites": STRING,
    "outcomes": [STRING,]*,
    "textbooks": [
        {
            "title": STRING,
            "link": STRING,
            "photo": STRING,
            "authors": [NAME,]*,
            "publisher": STRING,
            "year": YEAR
        }
    ],
    "gradedComponents": [
        {
            "name": NAME,
            "description": STRING,
            "weight": INT
        },
    ]*,
    "gradingNote": STRING,
    "academicDishonesty": STRING,
    "specialAssistance": STRING
    }
        */
         // CLEAR THE OLD DATA OUT
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(dir + "/" + SYLLABUS_DATA_JSON);
        SyllabusDetails syllabus = ((CSGData) dest).getSyllabusDetails();
        
        syllabus.setDescription(json.getString(DESCRIPTION));
        syllabus.setTopics(_writeString(json.getJsonArray(TOPICS)));
        syllabus.setPrereqs(json.getString(PREREQUISITES));
        syllabus.setOutcomes(_writeString(json.getJsonArray(OUTCOMES)));
        syllabus.setTextbooks(_writeString(json.getJsonArray(TEXTBOOKS)));
        syllabus.setGradedComponents(_writeString(json.getJsonArray(GRADED_COMPONENTS)));
        syllabus.setGradingNotes(json.getString(GRADING_NOTE));
        syllabus.setAcademicDishonesty(json.getString(ACADEMIC_DISHONESTY));
        syllabus.setSpecialAssistance(json.getString(SPECIAL_ASSISTANCE));
        
         dest.updateSyllabusDetails();
    }
    
    public void _pserializeOfficeHoursData(String dest, CSGData data) throws IOException {
        File f = new File(dest);
        
        if (!f.exists())
            f.mkdirs();
        // GET THE DATA
	CSGData dataManager = (CSGData)data;
	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder gradTAsArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder undergradTAsArrayBuilder = Json.createArrayBuilder();
	Iterator<TeachingAssistantPrototype> tasIterator = dataManager.teachingAssistantsIterator();
        while (tasIterator.hasNext()) {
            TeachingAssistantPrototype ta = tasIterator.next();
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_TYPE, ta.getType().toString()).build();
            if (ta.getType().equals(TAType.Graduate.toString()))
                gradTAsArrayBuilder.add(taJson);
            else
                undergradTAsArrayBuilder.add(taJson);
	}
        JsonArray gradTAsArray = gradTAsArrayBuilder.build();
	JsonArray undergradTAsArray = undergradTAsArrayBuilder.build();

	// NOW BUILD THE OFFICE HOURS JSON OBJCTS TO SAVE
	JsonArrayBuilder officeHoursArrayBuilder = Json.createArrayBuilder();
        Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
        while (timeSlotsIterator.hasNext()) {
            TimeSlot timeSlot = timeSlotsIterator.next();
            for (int i = 0; i < DayOfWeek.values().length; i++) {
                DayOfWeek dow = DayOfWeek.values()[i];
                tasIterator = timeSlot.getTAsIterator(dow);
                while (tasIterator.hasNext()) {
                    TeachingAssistantPrototype ta = tasIterator.next();
                    JsonObject tsJson = Json.createObjectBuilder()
                        .add(JSON_START_TIME, timeSlot.getStartTime().replace(":", "_"))
                        .add(JSON_DAY_OF_WEEK, dow.toString())
                        .add(JSON_NAME, ta.getName()).build();
                    officeHoursArrayBuilder.add(tsJson);
                }
            }
	}
	JsonArray officeHoursArray = officeHoursArrayBuilder.build();
        InstructorDetails instructor = data.getInstructorDetails();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, officeHoursArray)
                .add(INSTRUCTOR, Json.createObjectBuilder()
                    .add(NAME, instructor.getName())
                    .add(LINK, instructor.getWebsite())
                    .add(EMAIL, instructor.getEmail())
                    .add(ROOM, instructor.getRoom())
                    .add(HOURS, _stringReader(instructor.getOfficeHours()).readArray()).build())
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();
        
        String filePath = dest + "/" + OFFICE_HOURS_DATA_JSON;

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    public JsonReader _stringReader(String src) {
        return Json.createReader(new ByteArrayInputStream(src.getBytes(StandardCharsets.UTF_8)));
    }
    
    public void _pserializePageData(String dest, CSGData data) throws IOException {
        /*
        {
    "subject": SUBJECT,
    "number": NUMBER,
    "semester": SEMESTER,
    "year": YEAR,
    "title": TITLE,
    "logos": {
        "favicon": {
            "href": FAVICON
        },
        "navbar": {
            "href": NAVBAR,
            "src": NAVBAR
        },
        "bottom_left": {
            "href": BOTTOM-LEFT,
            "src": BOTTOM-LEFT
        },
        "bottom_right": {
            "href": BOTTOM-RIGHT,
            "src": BOTTOM-RIGHT
        }
    },
    "instructor": {
        		"name":    NAME,
        		"link":     WEBSITE,
        		"email":    EMAIL,
        		"room":     ROOM,
                                                                    "photo":    PATH,
        		"hours":    HOURS
	},
    "pages": [ {
		“name”: NAME,
		“link”: LINK
            }, ...]
	}
        */
        // GET THE DATA
        File f = new File(dest);
        
        if (!f.exists())
            f.mkdirs();
        data.editPageDetails();
        data.editInstructorDetails();
        
	JsonObjectBuilder builder = Json.createObjectBuilder();
        PageDetails page = data.getPageDetails();
        InstructorDetails instructor = data.getInstructorDetails();
        
        builder.add(SUBJECT, page.getSubject());
        builder.add(NUMBER, page.getNumber());
        builder.add(YEAR, page.getYear());
        builder.add(SEMESTER, page.getSemester());
        builder.add(TITLE, page.getTitle());
        builder.add(LOGOS, Json.createObjectBuilder()
                .add(FAVICON, Json.createObjectBuilder().add(HREF, page.getFaviconPath()).build())
                .add(NAVBAR, Json.createObjectBuilder().add(HREF, page.getNavbarPath()).add(SRC, page.getNavbarPath()).build())
                .add(LEFT_FOOTER, Json.createObjectBuilder().add(HREF, page.getLeftFooterPath()).add(SRC, page.getNavbarPath()).build())
                .add(RIGHT_FOOTER, Json.createObjectBuilder().add(HREF, page.getRightFooterPath()).add(SRC, page.getNavbarPath()).build()));
        builder.add(INSTRUCTOR, Json.createObjectBuilder()
                .add(NAME, instructor.getName())
                .add(LINK, instructor.getWebsite())
                .add(EMAIL, instructor.getEmail())
                .add(ROOM, instructor.getRoom())
                .add(HOURS, _stringReader(instructor.getOfficeHours()).readArray()).build());
        
        JsonArrayBuilder pagesBuilder = Json.createArrayBuilder();
        
        if (page.getIncludeHomePage())
            pagesBuilder.add(Json.createObjectBuilder().add(NAME, "Home").add(LINK, "index.html").build());
        
        if (page.getIncludeHWsPage())
            pagesBuilder.add(Json.createObjectBuilder().add(NAME, "HWs").add(LINK, "hws.html").build());
        
        if (page.getIncludeSchedulePage())
            pagesBuilder.add(Json.createObjectBuilder().add(NAME, "Schedule").add(LINK, "schedule.html").build());
        
        if (page.getIncludeSyllabusPage())
            pagesBuilder.add(Json.createObjectBuilder().add(NAME, "Syllabus").add(LINK, "syllabus.html").build());
        builder.add(PAGES, pagesBuilder.build());
        
        JsonObject built = builder.build();
        dest += "/" + PAGE_DATA_JSON;
        
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(built);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(dest);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(built);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(dest);
	pw.write(prettyPrinted);
	pw.close();
    }
    private static String LECTURES = "lectures";
    private static String RECITATIONS = "recitations";
    private static String LABS = "labs";
    private static String MONTH = "month";
    private static String DAY = "day";
    private static String TOPIC = "topic";
    private static String TIME = "time";
    private static String CRITERIA = "criteria";
    private static String HWS = "hws";
    private static String HOLIDAYS = "holidays";
    private static String REFERENCES = "references";
    private static String STARTING_MONDAY_MONTH = "startingMondayMonth";
    private static String STARTING_MONDAY_DAY = "startingMondayDay";
    private static String ENDING_FRIDAY_MONTH = "endingFridayMonth";
    private static String ENDING_FRIDAY_DAY = "endingFridayDay";
    public void _pserializeScheduleData(String dest, CSGData data) throws IOException  {
         /*
        {
            "startingMondayMonth":  INT,
            "startingMondayDay":    INT,
            "endingFridayMonth":    INT,
            "endingFridayDay":      INT,

            "holidays": [
                { "month": "12", "day": "21", "title": "", "link": "" }
            ],
        
            "labs":[
                        { "month": INT,     "day": INT,     "title": STRING,      "topic": STRING,    "link": STRING },
             ],

            "lectures":[
                { "month": INT,     "day": INT,     "title": STRING,      "topic": STRING,    "link": STRING}
            ],

            "references": [
                { "month": INT, "day": INT, "title": STRING, "topic": STRING, "link": STRING }
            ],

            "recitations":[
                        { "month": INT,     "day": INT,     "title": STRING,      "topic": STRING,    "link": STRING },
             ],
            "hws": [
                { "month": INT, "day": INT,    "title": STRING,    "topic": STRING,      "link": STRING,     "time": STRING,  "criteria": STRING     }
            ]
        }
        */
        File f = new File(dest);
        
        if (!f.exists())
            f.mkdirs();
        // GET THE DATA
	JsonObjectBuilder builder = Json.createObjectBuilder();
        
        builder.add(STARTING_MONDAY_MONTH, new Integer(LocalDate.parse(data.startingMonday).getMonthValue()).toString());
        builder.add(STARTING_MONDAY_DAY, new Integer(LocalDate.parse(data.startingMonday).getDayOfMonth()).toString());
        builder.add(ENDING_FRIDAY_MONTH, new Integer(LocalDate.parse(data.endingFriday).getMonthValue()).toString());
        builder.add(ENDING_FRIDAY_DAY, new Integer(LocalDate.parse(data.endingFriday).getDayOfMonth()).toString());
        
        List<LectureDetails> lectures = data.getLectures();
        List<RecitationDetails> recitations = data.getRecitations();
        List<LabDetails> labs = data.getLabs();
        
        JsonArrayBuilder lectureBuilder = Json.createArrayBuilder();
        JsonArrayBuilder recitationBuilder = Json.createArrayBuilder();
        JsonArrayBuilder labBuilder = Json.createArrayBuilder();
       JsonArrayBuilder holidaysBuilder = Json.createArrayBuilder();
        JsonArrayBuilder referencesBuilder = Json.createArrayBuilder();
        JsonArrayBuilder hwsBuilder = Json.createArrayBuilder();
        
        for (ScheduleItemPrototype i : (List<ScheduleItemPrototype>) data.getScheduleItems()) {
            JsonObjectBuilder _objBuilder = Json.createObjectBuilder();
            _objBuilder.add(MONTH, new Integer(LocalDate.parse(i.date.get()).getMonthValue()).toString());
            _objBuilder.add(DAY, new Integer(LocalDate.parse(i.date.get()).getDayOfMonth()).toString());
            _objBuilder.add(TITLE, i.title.get());
            _objBuilder.add(TOPIC, i.topic.get());
            _objBuilder.add(LINK, "");
            switch (i.type.get()) {
                case "Holiday":
                    holidaysBuilder.add(_objBuilder.build());
                    break;
                case "HW":
                    _objBuilder.add(CRITERIA, "");
                    _objBuilder.add(TIME, "");
                    hwsBuilder.add(_objBuilder.build());
                    break;
                case "Lab":
                    labBuilder.add(_objBuilder.build());
                    break;
                case "Lecture":
                    lectureBuilder.add(_objBuilder.build());
                    break;
                case "Recitation":
                    recitationBuilder.add(_objBuilder.build());
                case "Reference":
                    referencesBuilder.add(_objBuilder.build());
            }
        }
        
        builder.add(LECTURES, lectureBuilder.build())
                .add(RECITATIONS, recitationBuilder.build())
                .add(LABS, labBuilder.build())
                .add(HWS, hwsBuilder.build())
                .add(HOLIDAYS, holidaysBuilder.build())
                .add(REFERENCES, referencesBuilder.build());
        
        JsonObject built = builder.build();
        dest += "/" + SCHEDULE_DATA_JSON;
        
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(built);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(dest);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(built);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(dest);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    public void _pserializeSyllabusData(String dest, CSGData data) throws IOException {
        /*
        {
    "description": STRING,
    "topics": [STRING,]*,
    "prerequisites": STRING,
    "outcomes": [STRING,]*,
    "textbooks": [
        {
            "title": STRING,
            "link": STRING,
            "photo": STRING,
            "authors": [NAME,]*,
            "publisher": STRING,
            "year": YEAR
        }
    ],
    "gradedComponents": [
        {
            "name": NAME,
            "description": STRING,
            "weight": INT
        },
    ]*,
    "gradingNote": STRING,
    "academicDishonesty": STRING,
    "specialAssistance": STRING
    }
        */
        File f = new File(dest);
        
        if (!f.exists())
            f.mkdirs();
        data.editSyllabusDetails();
        
        // GET THE DATA
	JsonObjectBuilder builder = Json.createObjectBuilder();
        SyllabusDetails syllabus = data.getSyllabusDetails();
        
        builder.add(DESCRIPTION, syllabus.getDescription());
        builder.add(TOPICS, _stringReader(syllabus.getTopics()).readArray());
        builder.add(PREREQUISITES, syllabus.getPrereqs());
        builder.add(OUTCOMES, _stringReader(syllabus.getOutcomes()).readArray());
        builder.add(TEXTBOOKS, _stringReader(syllabus.getTextbooks()).readArray());
        builder.add(GRADED_COMPONENTS, _stringReader(syllabus.getGradedComponents()).readArray());
        builder.add(GRADING_NOTE, syllabus.getGradingNotes());
        builder.add(ACADEMIC_DISHONESTY, syllabus.getAcademicDishonesty());
        builder.add(SPECIAL_ASSISTANCE, syllabus.getSpecialAssistance());
        
        JsonObject built = builder.build();
        dest += "/" + SYLLABUS_DATA_JSON;
        
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(built);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(dest);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(built);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(dest);
	pw.write(prettyPrinted);
	pw.close();
    }
}