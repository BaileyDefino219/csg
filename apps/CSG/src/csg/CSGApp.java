package csg;

import djf.AppTemplate;
import djf.components.AppClipboardComponent;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import djf.components.AppWorkspaceComponent;
import java.util.Locale;
import csg.data.CSGData;
import csg.files.CSGFiles;
import csg.clipboard.CSGClipboard;
import static csg.files.CSGFiles.STATE;
import csg.workspace.CSGWorkspace;
import java.io.IOException;
import static javafx.application.Application.launch;

public class CSGApp extends AppTemplate {   
    /**
     * This is where program execution begins. Since this is a JavaFX app it
     * will simply call launch, which gets JavaFX rolling, resulting in sending
     * the properly initialized Stage (i.e. window) to the start method inherited
     * from AppTemplate, defined in the Desktop Java Framework.
     * 
     * @param args Command-line arguments, there are no such settings used
     * by this application.
     */
    public static void main(String[] args) {
	Locale.setDefault(Locale.US);
	launch(args);
    }

    @Override
    public AppClipboardComponent buildClipboardComponent(AppTemplate app) {
        
        CSGClipboard clipboard = new CSGClipboard(this);
        // this is the last-built component, so we call this here because we can't override a private method
        CSGData data = (CSGData) app.getDataComponent();
        try { // attempts to load
            ((CSGFiles) fileComponent)._pdserializePageData(data, CSGFiles.STATE);
        } catch (IOException e) { // use the default data
            data.updateInstructorDetails();
            data.updatePageDetails();
        }
        data.updateSyllabusDetails();
        djf.modules.AppGUIModule gui = app.getGUIModule();
        data.startingMonday = ((javafx.scene.control.DatePicker) gui.getGUINode(
                csg.CSGPropertyType.CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_START_DATE_PICKER)).getValue().toString();
        data.endingFriday = ((javafx.scene.control.DatePicker) gui.getGUINode(
                csg.CSGPropertyType.CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_END_DATE_PICKER)).getValue().toString();
        return clipboard;
    }

    @Override
    public AppDataComponent buildDataComponent(AppTemplate app) {
        return new CSGData(this);
    }

    @Override
    public AppFileComponent buildFileComponent() {
        return new CSGFiles(this);
    }

    @Override
    public AppWorkspaceComponent buildWorkspaceComponent(AppTemplate app) {
        return new CSGWorkspace(this);
    }
    
    @Override
    public void stop() throws Exception {
        CSGData data = (CSGData) dataComponent;
        ((CSGFiles) fileComponent)._pserializePageData(CSGFiles.STATE, data);
        super.stop();
    }
}