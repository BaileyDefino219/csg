/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CSGPropertyType;
import static csg.CSGPropertyType.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import java.util.ArrayList;
/**
 *
 * @author hiten
 */
public class LectureDetails {
    public ArrayList<ScheduleItemPrototype> children;
    public StringProperty section;
        public StringProperty days;
        public StringProperty time;
        public StringProperty room;
        public LectureDetails() {
            section = new SimpleStringProperty();
            days = new SimpleStringProperty();
            time = new SimpleStringProperty();
            room = new SimpleStringProperty();
            children = new ArrayList<>();
        }
        public LectureDetails(String days, String room, String section, String time) {
            this.days = new SimpleStringProperty(days);
            this.room = new SimpleStringProperty(room);
            this.section = new SimpleStringProperty(section);
            this.time = new SimpleStringProperty(time);
            children = new ArrayList<>();
        }
        public void load(LectureDetails o) {
            days.set(o.days.get());
            room.set(o.room.get());
            section.set(o.section.get());
            time.set(o.time.get());
            children.clear();
            children.addAll(o.children);
        }
        public StringProperty daysProperty() {
            return days;
        }
        public StringProperty roomProperty() {
            return room;
        }
        public StringProperty sectionProperty() {
            return section;
        }
        public StringProperty timeProperty() {
            return time;
        }
}
