/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CSGPropertyType;
import static csg.CSGPropertyType.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import java.util.ArrayList;
/**
 *
 * @author hiten
 */
public class LabDetails {
    public ArrayList<ScheduleItemPrototype> children;
    public StringProperty section;
        public StringProperty daysTime;
        public StringProperty room;
        public StringProperty tas;
        public LabDetails() {
            section = new SimpleStringProperty();
            daysTime = new SimpleStringProperty();
            room = new SimpleStringProperty();
            tas = new SimpleStringProperty();
            children = new ArrayList<>();
        }
        public LabDetails(String daysTime, String room, String section, String tas) {
            this.daysTime = new SimpleStringProperty(daysTime);
            this.room = new SimpleStringProperty(room);
            this.section = new SimpleStringProperty(section);
            this.tas = new SimpleStringProperty(tas);
            children = new ArrayList<>();
        }
        public void load(LabDetails o) {
            daysTime.set(o.daysTime.get());
            room.set(o.room.get());
            section.set(o.section.get());
            tas.set(o.tas.get());
            children.clear();
            children.addAll(o.children);
        }
        public StringProperty daysTimeProperty() {
            return daysTime;
        }
        public StringProperty roomProperty() {
            return room;
        }
        public StringProperty sectionProperty() {
            return section;
        }
        public StringProperty tasProperty() {
            return tas;
        }
}
