/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;
import csg.CSGPropertyType;
import static csg.CSGPropertyType.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 *
 * @author hiten
 */
public class PageDetails {
    /* primary storage for webpage details */
    public static CSGPropertyType[] InputProperties = new CSGPropertyType[] { // these hold pertinent data, most of which also fire events
        CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SUBJECT_HBOX_COMBOBOX,
        CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_NUMBER_HBOX_COMBOBOX,
        CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SEMESTER_HBOX_COMBOBOX,
        CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_YEAR_HBOX_COMBOBOX,
        CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_TITLE_HBOX_TEXT_FIELD,
        CSG_SITE_SPLIT_PANE_PAGES_HBOX_HOME_CHECKBOX,
        CSG_SITE_SPLIT_PANE_PAGES_HBOX_HWS_CHECKBOX,
        CSG_SITE_SPLIT_PANE_PAGES_HBOX_SCHEDULE_CHECKBOX,
        CSG_SITE_SPLIT_PANE_PAGES_HBOX_SYLLABUS_CHECKBOX,
        CSG_SITE_SPLIT_PANE_STYLE_VBOX_FAVICON_HBOX_BUTTON,
        CSG_SITE_SPLIT_PANE_STYLE_VBOX_NAVBAR_HBOX_BUTTON,
        CSG_SITE_SPLIT_PANE_STYLE_VBOX_LEFT_FOOTER_HBOX_BUTTON,
        CSG_SITE_SPLIT_PANE_STYLE_VBOX_RIGHT_FOOTER_HBOX_BUTTON,
        CSG_SITE_SPLIT_PANE_STYLE_VBOX_STYLESHEET_HBOX_CHOOSER
    };
    public StringProperty subject;
    public IntegerProperty number;
    public StringProperty semester;
    public IntegerProperty year;
    
    public StringProperty title;
    public StringProperty exportDir;
    
    public BooleanProperty includeHomePage;
    public BooleanProperty includeHWsPage;
    public BooleanProperty includeSchedulePage;
    public BooleanProperty includeSyllabusPage;
    
    public StringProperty faviconPath;
    public StringProperty navbarPath;
    public StringProperty leftFooterPath;
    public StringProperty rightFooterPath;
    
    public StringProperty stylesheetPath;
    
    public PageDetails() {
        this.subject = new SimpleStringProperty("AAS");
        this.number = new SimpleIntegerProperty(100);
        this.semester = new SimpleStringProperty("Fall");
        this.year = new SimpleIntegerProperty(2018);
        
        this.title = new SimpleStringProperty("");
        this.exportDir = new SimpleStringProperty("./export/AAS_100_Fall_2018/");
        
        this.includeHomePage = new SimpleBooleanProperty(false);
        this.includeHWsPage = new SimpleBooleanProperty(false);
        this.includeSchedulePage = new SimpleBooleanProperty(false);
        this.includeSyllabusPage = new SimpleBooleanProperty(false);
        
        this.faviconPath = new SimpleStringProperty("./images/Favicon.ico");
        this.navbarPath = new SimpleStringProperty("./images/Navbar.png");
        this.leftFooterPath = new SimpleStringProperty("./images/Left Footer.png");
        this.rightFooterPath = new SimpleStringProperty("./images/Right Footer.png");
        
        this.stylesheetPath = new SimpleStringProperty("./work/style/default.css");
    }
    
    public PageDetails(String subject, int number, String semester, int year, String title, String exportDir, boolean includeHomePage, boolean includeHWsPage,
            boolean includeSchedulePage, boolean includeSyllabusPage, String faviconPath, String navbarPath, String leftFooterPath, String rightFooterPath,
            String stylesheetPath) {
        this.subject = new SimpleStringProperty(subject);
        this.number = new SimpleIntegerProperty(number);
        this.semester = new SimpleStringProperty(semester);
        this.year = new SimpleIntegerProperty(year);
        
        this.title = new SimpleStringProperty(title);
        this.exportDir = new SimpleStringProperty(exportDir);
        
        this.includeHomePage = new SimpleBooleanProperty(includeHomePage);
        this.includeHWsPage = new SimpleBooleanProperty(includeHWsPage);
        this.includeSchedulePage = new SimpleBooleanProperty(includeSchedulePage);
        this.includeSyllabusPage = new SimpleBooleanProperty(includeSyllabusPage);
        
        this.faviconPath = new SimpleStringProperty(faviconPath);
        this.navbarPath = new SimpleStringProperty(navbarPath);
        this.leftFooterPath = new SimpleStringProperty(leftFooterPath);
        this.rightFooterPath = new SimpleStringProperty(rightFooterPath);
        
        this.stylesheetPath = new SimpleStringProperty(stylesheetPath);
    }
    
    public void deserialize(String src) {
        
    }
    
    public void load(PageDetails other) {
        this.subject.set(other.getSubject());
        this.number.set(other.getNumber());
        this.semester.set(other.getSemester());
        this.year.set(other.getYear());
        
        this.title.set(other.getTitle());
        this.exportDir.set(other.getExportDir());
        
        this.includeHomePage.set(other.getIncludeHomePage());
        this.includeHWsPage.set(other.getIncludeHWsPage());
        this.includeSchedulePage.set(other.getIncludeSchedulePage());
        this.includeSyllabusPage.set(other.getIncludeSyllabusPage());
        
        this.faviconPath.set(other.getFaviconPath());
        this.navbarPath.set(other.getNavbarPath());
        this.leftFooterPath.set(other.getLeftFooterPath());
        this.rightFooterPath.set(other.getRightFooterPath());
        
        this.stylesheetPath.set(other.getStylesheetPath());
    }
    
    public String serialize() {
        return null;
    }
    ///////////////////////////////accessors/mutators
    public String getSubject() {
        return this.subject.get();
    }
    public int getNumber() {
        return this.number.get();
    }
    public String getSemester() {
        return this.semester.get();
    }
    public int getYear() {
        return this.year.get();
    }
    public String getTitle() {
        return this.title.get();
    }
    public String getExportDir() {
        return this.exportDir.get();
    }
    public boolean getIncludeHomePage() {
        return this.includeHomePage.get();
    }
    public boolean getIncludeHWsPage() {
        return this.includeHWsPage.get();
    }
    public boolean getIncludeSchedulePage() {
        return this.includeSchedulePage.get();
    }
    public boolean getIncludeSyllabusPage() {
        return this.includeSyllabusPage.get();
    }
    public String getFaviconPath() {
        return this.faviconPath.get();
    }
    public String getNavbarPath() {
        return this.navbarPath.get();
    }
    public String getLeftFooterPath() {
        return this.leftFooterPath.get();
    }
    public String getRightFooterPath() {
        return this.rightFooterPath.get();
    }
    public String getStylesheetPath() {
        return this.stylesheetPath.get();
    }
    
    public void setSubject(String subject) {
        this.subject.set(subject);
    }
    public void setNumber(int number) {
        this.number.set(number);
    }
    public void setSemester(String semester) {
        this.semester.set(semester);
    }
    public void setYear(int year) {
        this.year.set(year);
    }
    public void setTitle(String title) {
        this.title.set(title);
    }
    public void setExportDir(String exportDir) {
        this.exportDir.set(exportDir);
    }
    public void setIncludeHomePage(boolean includeHomePage) {
        this.includeHomePage.set(includeHomePage);
    }
    public void setIncludeHWsPage(boolean includeHWsPage) {
        this.includeHWsPage.set(includeHWsPage);
    }
    public void setIncludeSchedulePage(boolean includeSchedulePage) {
        this.includeSchedulePage.set(includeSchedulePage);
    }
    public void setIncludeSyllabusPage(boolean includeSyllabusPage) {
        this.includeSyllabusPage.set(includeSyllabusPage);
    }
    public void setFaviconPath(String faviconPath) {
        this.faviconPath.set(faviconPath);
    }
    public void setNavbarPath(String navbarPath) {
        this.navbarPath.set(navbarPath);
    }
    public void setLeftFooterPath(String leftFooterPath) {
        this.leftFooterPath.set(leftFooterPath);
    }
    public void setRightFooterPath(String rightFooterPath) {
        this.rightFooterPath.set(rightFooterPath);
    }
    public void setStylesheetPath(String stylesheetPath) {
        this.stylesheetPath.set(stylesheetPath);
    }
    //////////////////////property accessors/mutators
    public StringProperty getSubjectProperty() {
        return this.subject;
    }
    public IntegerProperty getNumberProperty() {
        return this.number;
    }
    public StringProperty getSemesterProperty() {
        return this.semester;
    }
    public IntegerProperty getYearProperty() {
        return this.year;
    }
    public StringProperty getTitleProperty() {
        return this.title;
    }
    public StringProperty getExportDirProperty() {
        return this.exportDir;
    }
    public BooleanProperty getIncludeHomePageProperty() {
        return this.includeHomePage;
    }
    public BooleanProperty getIncludeHWsPageProperty() {
        return this.includeHWsPage;
    }
    public BooleanProperty getIncludeSchedulePageProperty() {
        return this.includeSchedulePage;
    }
    public BooleanProperty getIncludeSyllabusPageProperty() {
        return this.includeSyllabusPage;
    }
    public StringProperty getFaviconPathProperty() {
        return this.faviconPath;
    }
    public StringProperty getNavbarPathProperty() {
        return this.navbarPath;
    }
    public StringProperty getLeftFooterPathProperty() {
        return this.leftFooterPath;
    }
    public StringProperty getRightFooterPathProperty() {
        return this.rightFooterPath;
    }
    public StringProperty getStylesheetPathProperty() {
        return this.stylesheetPath;
    }
    
    public void setSubjectProperty(StringProperty subject) {
        this.subject = subject;
    }
    public void setNumberProperty(IntegerProperty number) {
        this.number = number;
    }
    public void setSemesterProperty(StringProperty semester) {
        this.semester = semester;
    }
    public void setYearProperty(IntegerProperty year) {
        this.year = year;
    }
    public void setTitleProperty(StringProperty title) {
        this.title = title;
    }
    public void setExportDirProperty(StringProperty exportDir) {
        this.exportDir = exportDir;
    }
    public void setIncludeHomePageProperty(BooleanProperty includeHomePage) {
        this.includeHomePage = includeHomePage;
    }
    public void setIncludeHWsPageProperty(BooleanProperty includeHWsPage) {
        this.includeHWsPage = includeHWsPage;
    }
    public void setIncludeSchedulePageProperty(BooleanProperty includeSchedulePage) {
        this.includeSchedulePage = includeSchedulePage;
    }
    public void setIncludeSyllabusPageProperty(BooleanProperty includeSyllabusPage) {
        this.includeSyllabusPage = includeSyllabusPage;
    }
    public void setFaviconPathProperty(StringProperty faviconPath) {
        this.faviconPath = faviconPath;
    }
    public void setNavbarPathProperty(StringProperty navbarPath) {
        this.navbarPath = navbarPath;
    }
    public void setLeftFooterPathProperty(StringProperty leftFooterPath) {
        this.leftFooterPath = leftFooterPath;
    }
    public void setRightFooterPathProperty(StringProperty rightFooterPath) {
        this.rightFooterPath = rightFooterPath;
    }
    public void setStylesheetPathProperty(StringProperty stylesheetPath) {
        this.stylesheetPath = stylesheetPath;
    }
}
