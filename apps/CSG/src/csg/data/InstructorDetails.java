/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CSGPropertyType;
import static csg.CSGPropertyType.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author hiten
 */
public class InstructorDetails {
    public static CSGPropertyType[] InputProperties = new CSGPropertyType[] { // these hold pertinent data, most of which also fire events

        CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_NAME_HBOX_TEXT_FIELD,
        CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_ROOM_HBOX_TEXT_FIELD,
        CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_EMAIL_HBOX_TEXT_FIELD,
        CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_WEBSITE_HBOX_TEXT_FIELD,
        CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_OFFICE_HOURS_VBOX_TEXT_AREA
    };
    public StringProperty name;
    public StringProperty room;
    public StringProperty email;
    public StringProperty website;
    
    public StringProperty officeHours; // JSON string
    
    public InstructorDetails() {
        this.name = new SimpleStringProperty("");
        this.room = new SimpleStringProperty("");
        this.email = new SimpleStringProperty("");
        this.website = new SimpleStringProperty("");
        
        this.officeHours = new SimpleStringProperty("[\n{\n\"day\": \"Monday\",\n\"time\": \"8AM-10AM\"\n}\n]");
    }
    
    public InstructorDetails(String name, String room, String email, String website, String officeHours) {
        this.name = new SimpleStringProperty(name);
        this.room = new SimpleStringProperty(room);
        this.email = new SimpleStringProperty(email);
        this.website = new SimpleStringProperty(website);
        
        this.officeHours = new SimpleStringProperty(officeHours);
    }
    
    public void deserialize(String src) {
        
    }
    
    
    public void load(InstructorDetails other) {
        this.name = new SimpleStringProperty(other.getName());
        this.room = new SimpleStringProperty(other.getRoom());
        this.email = new SimpleStringProperty(other.getEmail());
        this.website = new SimpleStringProperty(other.getWebsite());
        
        this.officeHours = new SimpleStringProperty(other.getOfficeHours());
    }
    
    public String serialize() {
        return null;
    }
    
    public String getName() {
        return this.name.get();
    }
    public String getRoom() {
        return this.room.get();
    }
    public String getEmail() {
        return this.email.get();
    }
    public String getWebsite() {
        return this.website.get();
    }
    public String getOfficeHours() {
        return this.officeHours.get();
    }
    
    public void setName(String instructorName) {
        this.name.set(instructorName);
    }
    public void setRoom(String instructorRoom) {
        this.room.set(instructorRoom);
    }
    public void setEmail(String instructorEmail) {
        this.email.set(instructorEmail);
    }
    public void setWebsite(String instructorWebsite) {
        this.website.set(instructorWebsite);
    }
    public void setOfficeHours(String instructorOfficeHours) {
        this.officeHours.set(instructorOfficeHours);
    }
    
    ///////////////
    
    public StringProperty getNameProperty() {
        return this.name;
    }
    public StringProperty getRoomProperty() {
        return this.room;
    }
    public StringProperty getEmailProperty() {
        return this.email;
    }
    public StringProperty getWebsiteProperty() {
        return this.website;
    }
    public StringProperty getOfficeHoursProperty() {
        return this.officeHours;
    }
    
    public void setNameProperty(StringProperty instructorName) {
        this.name = instructorName;
    }
    public void setRoomProperty(StringProperty instructorRoom) {
        this.room = instructorRoom;
    }
    public void setEmailProperty(StringProperty instructorEmail) {
        this.email = instructorEmail;
    }
    public void setWebsiteProperty(StringProperty instructorWebsite) {
        this.website = instructorWebsite;
    }
    public void setOfficeHoursProperty(StringProperty instructorOfficeHours) {
        this.officeHours = instructorOfficeHours;
    }
}
