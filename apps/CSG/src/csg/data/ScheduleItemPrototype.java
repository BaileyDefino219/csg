/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 *
 * @author hiten
 */
public class ScheduleItemPrototype {
    public StringProperty type;
    public StringProperty date;
    public StringProperty title;
    public StringProperty topic;
    public ScheduleItemPrototype() {
        date = new SimpleStringProperty();
        title = new SimpleStringProperty();
        topic = new SimpleStringProperty();
        type = new SimpleStringProperty();
    }
    public ScheduleItemPrototype(String date, String title, String topic, String type) {
        this.date = new SimpleStringProperty(date);
        this.title = new SimpleStringProperty(title);
        this.topic = new SimpleStringProperty(topic);
        this.type = new SimpleStringProperty(type);
    }
    public void load(ScheduleItemPrototype other) {
        date = new SimpleStringProperty(other.date.get());
        title = new SimpleStringProperty(other.title.get());
        topic = new SimpleStringProperty(other.topic.get());
        type = new SimpleStringProperty(other.type.get());
    }
    public StringProperty dateProperty() {
        return date;
    }
    public StringProperty titleProperty() {
        return title;
    }
    public StringProperty topicProperty() {
        return topic;
    }
    public StringProperty typeProperty() {
        return type;
    }
}
