/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;
import java.time.LocalDate;
import java.time.DayOfWeek;
/**
 *
 * @author hiten
 */
public class DOWTranslator {
    /* determine the next specific weekday including or after a date */
    public static LocalDate next(LocalDate start, DayOfWeek day) {
        LocalDate res = LocalDate.parse(start.toString());
        while (!res.getDayOfWeek().equals(day))
            res.plusDays(1);
        return res;
    }
    public static String extractDays(String daytimestring) {
        if (!daytimestring.contains("@")) {
            if (daytimestring.contains(":"))
                return "";
            return daytimestring;
        }
        return daytimestring.substring(0, daytimestring.indexOf('@'));
    }
    public static DayOfWeek[] timestring2days(String timestring) {
        java.util.ArrayList<DayOfWeek> days = new java.util.ArrayList<>();
        timestring = timestring.toUpperCase();
        for (int i = 0; i < timestring.length(); i++) {
            switch (timestring.charAt(i)) { // ignore unknowns
                case 'M':
                    days.add(DayOfWeek.MONDAY);
                    break;
                case 'T':
                    if (days.contains(DayOfWeek.TUESDAY))
                        days.add(DayOfWeek.THURSDAY);
                    else
                        days.add(DayOfWeek.TUESDAY);
                    break;
                case 'W':
                    days.add(DayOfWeek.WEDNESDAY);
                    break;
                case 'F':
                    days.add(DayOfWeek.FRIDAY);
            }
        }
        return days.toArray(new DayOfWeek[days.size()]);
    }
}
