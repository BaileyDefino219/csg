/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CSGPropertyType;
import static csg.CSGPropertyType.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author hiten
 */
public class SyllabusDetails {
    public static CSGPropertyType[] InputProperties = new CSGPropertyType[] {
        CSG_SYLLABUS_SPLIT_PANE_DESCRIPTION_VBOX_TEXT_AREA,
        CSG_SYLLABUS_SPLIT_PANE_TOPICS_VBOX_TEXT_AREA,
        CSG_SYLLABUS_SPLIT_PANE_PREREQS_VBOX_TEXT_AREA,
        CSG_SYLLABUS_SPLIT_PANE_OUTCOMES_VBOX_TEXT_AREA,
        CSG_SYLLABUS_SPLIT_PANE_TEXTBOOKS_VBOX_TEXT_AREA,
        CSG_SYLLABUS_SPLIT_PANE_GRADED_COMPONENTS_VBOX_TEXT_AREA,
        CSG_SYLLABUS_SPLIT_PANE_GRADING_NOTES_VBOX_TEXT_AREA,
        CSG_SYLLABUS_SPLIT_PANE_ACADEMIC_DISHONESTY_VBOX_TEXT_AREA,
        CSG_SYLLABUS_SPLIT_PANE_SPECIAL_ASSISTANCE_VBOX_TEXT_AREA
    };
    public StringProperty academicDishonesty;
    public StringProperty description;
    public StringProperty gradedComponents;
    public StringProperty gradingNotes;
    public StringProperty outcomes;
    public StringProperty prereqs;
    public StringProperty specialAssistance;
    public StringProperty textbooks;
    public StringProperty topics;
    
    public SyllabusDetails() {
        academicDishonesty = new SimpleStringProperty("");
        description = new SimpleStringProperty("");
        gradedComponents = new SimpleStringProperty("[\n{\n\"name\": \"\",\n\"description\": \"\",\n\"weight\": 0\n}\n]");
        gradingNotes = new SimpleStringProperty("");
        outcomes = new SimpleStringProperty("[]");
        prereqs = new SimpleStringProperty("");
        specialAssistance = new SimpleStringProperty("");
        textbooks = new SimpleStringProperty("[\n{\n\"title\": \"\",\n\"link\": \"\",\n\"photo\": \"\",\n\"authors\": [],\n\"publisher\": \"\",\n\"year\": 2018\n}\n]");
        topics = new SimpleStringProperty("[]");
    }
    
    public SyllabusDetails(String academicDishonesty, String description, String gradedComponents,
            String gradingNotes, String outcomes, String prereqs, String specialAssistance,
            String textbooks, String topics) {
        this.academicDishonesty = new SimpleStringProperty(academicDishonesty);
        this.description = new SimpleStringProperty(description);
        this.gradedComponents = new SimpleStringProperty(gradedComponents);
        this.gradingNotes = new SimpleStringProperty(gradingNotes);
        this.outcomes = new SimpleStringProperty(outcomes);
        this.prereqs = new SimpleStringProperty(prereqs);
        this.specialAssistance = new SimpleStringProperty(specialAssistance);
        this.textbooks = new SimpleStringProperty(textbooks);
        this.topics = new SimpleStringProperty(topics);
    }
    
    public void deserialize(String src) {
        
    }
    
    public void load(SyllabusDetails other) {
        this.academicDishonesty.set(other.getAcademicDishonesty());
        this.description.set(other.getDescription());
        this.gradedComponents.set(other.getGradedComponents());
        this.gradingNotes.set(other.getGradingNotes());
        this.outcomes.set(other.getOutcomes());
        this.prereqs.set(other.getPrereqs());
        this.specialAssistance.set(other.getSpecialAssistance());
        this.textbooks.set(other.getTextbooks());
        this.topics.set(other.getTopics());
    }
    
    public String serialize() {
        return null;
    }
    
    public String getAcademicDishonesty() {
        return this.academicDishonesty.get();
    }
    public String getDescription() {
        return this.description.get();
    }
    public String getGradedComponents() {
        return this.gradedComponents.get();
    }
    public String getGradingNotes() {
        return this.gradingNotes.get();
    }
    public String getOutcomes() {
        return this.outcomes.get();
    }
    public String getPrereqs() {
        return this.prereqs.get();
    }
    public String getSpecialAssistance() {
        return this.specialAssistance.get();
    }
    public String getTextbooks() {
        return this.textbooks.get();
    }
    public String getTopics() {
        return this.topics.get();
    }

    public StringProperty getAcademicDishonestyProperty() {
        return this.academicDishonesty;
    }
    public StringProperty getDescriptionProperty() {
        return this.description;
    }
    public StringProperty getGradedComponentsProperty() {
        return this.gradedComponents;
    }
    public StringProperty getGradingNotesProperty() {
        return this.gradingNotes;
    }
    public StringProperty getOutcomesProperty() {
        return this.outcomes;
    }
    public StringProperty getPrereqsProperty() {
        return this.prereqs;
    }
    public StringProperty getSpecialAssistanceProperty() {
        return this.specialAssistance;
    }
    public StringProperty getTextbooksProperty() {
        return this.textbooks;
    }
    public StringProperty getTopicsProperty() {
        return this.topics;
    }
    
    public void setAcademicDishonesty(String _new) {
        this.academicDishonesty.set(_new);
    }
    public void setDescription(String _new) {
        this.description.set(_new);
    }
    public void setGradedComponents(String _new) {
        this.gradedComponents.set(_new);
    }
    public void setGradingNotes(String _new) {
        this.gradingNotes.set(_new);
    }
    public void setOutcomes(String _new) {
        this.outcomes.set(_new);
    }
    public void setPrereqs(String _new) {
        this.prereqs.set(_new);
    }
    public void setSpecialAssistance(String _new) {
        this.specialAssistance.set(_new);
    }
    public void setTextbooks(String _new) {
        this.textbooks.set(_new);
    }
    public void setTopics(String _new) {
        this.topics.set(_new);
    }
}
