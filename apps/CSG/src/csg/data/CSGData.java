package csg.data;

import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Consumer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import csg.CSGApp;
import static csg.CSGPropertyType.*;
import csg.data.*;
import csg.data.TimeSlot.DayOfWeek;
import java.util.HashSet;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * This is the data component for TAManagerApp. It has all the data needed
 * to be set by the user via the User Interface and file I/O can set and get
 * all the data from this object
 * 
 * @author Richard McKenna
 */
public class CSGData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    CSGApp app;
    
    // THESE ARE ALL THE TEACHING ASSISTANTS
    HashMap<TAType, ArrayList<TeachingAssistantPrototype>> allTAs;
    ArrayList<TimeSlot> allSlots;
    
    PageDetails pageDetails;
    InstructorDetails instructorDetails;
    SyllabusDetails syllabusDetails;
    ObservableList<LectureDetails> lectureDetails;
    ObservableList<RecitationDetails> recitationDetails;
    ObservableList<LabDetails> labDetails;
    ObservableList<ScheduleItemPrototype> scheduleItems;
    
    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<TeachingAssistantPrototype> teachingAssistants;
    ObservableList<TimeSlot> officeHours;
    
    public String startingMonday;
    public String endingFriday;

    // THESE ARE THE TIME BOUNDS FOR THE OFFICE HOURS GRID. NOTE
    // THAT THESE VALUES CAN BE DIFFERENT FOR DIFFERENT FILES, BUT
    // THAT OUR APPLICATION USES THE DEFAULT TIME VALUES AND PROVIDES
    // NO MEANS FOR CHANGING THESE VALUES
    int startHour;
    int endHour;
    
    // DEFAULT VALUES FOR START AND END HOURS IN MILITARY HOURS
    public static final int MIN_START_HOUR = 9;
    public static final int MAX_END_HOUR = 20;

    /**
     * This constructor will setup the required data structures for
     * use, but will have to wait on the office hours grid, since
     * it receives the StringProperty objects from the Workspace.
     * 
     * @param initApp The application this data manager belongs to. 
     */
    public CSGData(CSGApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        AppGUIModule gui = app.getGUIModule();

        // SETUP THE DATA STRUCTURES
        allTAs = new HashMap();
        allTAs.put(TAType.Graduate, new ArrayList());
        allTAs.put(TAType.Undergraduate, new ArrayList());
        
        allSlots = new ArrayList<>();
        
        pageDetails = new PageDetails();
        instructorDetails = new InstructorDetails();
        syllabusDetails = new SyllabusDetails();
        lectureDetails = ((TableView) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LECTURES_TABLE_VIEW)).getItems();
        recitationDetails = ((TableView) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_RECITATIONS_TABLE_VIEW)).getItems();
        labDetails = ((TableView) gui.getGUINode(CSG_MEETING_TIMES_SPLIT_PANE_LABS_TABLE_VIEW)).getItems();
        scheduleItems = ((TableView) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_SCHEDULE_VBOX_TABLE_VIEW)).getItems();
        
        // GET THE LIST OF TAs FOR THE LEFT TABLE
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(CSG_OFFICE_HOURS_TAS_TABLE_VIEW);
        teachingAssistants = taTableView.getItems();

        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        
        resetCSG();
    }
    
    // ACCESSOR METHODS

    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }
    
    // PRIVATE HELPER METHODS
    
    private void sortTAs() {
        Collections.sort(teachingAssistants);
    }
    
    private void resetCSG() {
        //THIS WILL STORE OUR OFFICE HOURS
        
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView)gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        officeHours = officeHoursTableView.getItems(); 
        officeHours.clear();
        for (int i = startHour; i <= endHour; i++) {
            TimeSlot timeSlot = new TimeSlot(   this.getTimeString(i, true),
                                                this.getTimeString(i, false));
            officeHours.add(timeSlot);
            allSlots.add(timeSlot);
            
            TimeSlot halfTimeSlot = new TimeSlot(   this.getTimeString(i, false),
                                                    this.getTimeString(i+1, true));
            officeHours.add(halfTimeSlot);
            allSlots.add(halfTimeSlot);
        }
    }
    
    private String getTimeString(int militaryHour, boolean onHour) {
        String minutesText = "00";
        if (!onHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
    // METHODS TO OVERRIDE
        
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    @Override
    public void reset() {
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        pageDetails.load(new PageDetails());
        //updatePageDetails();
        instructorDetails.load(new InstructorDetails());
        //updateInstructorDetails();
        syllabusDetails.load(new SyllabusDetails());
        //updateSyllabusDetails();
        teachingAssistants.clear();
        
        for (TimeSlot timeSlot : officeHours) {
            timeSlot.reset();
        }
    }
    
    // SERVICE METHODS
    
    public void initHours(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        if (initStartHour <= initEndHour) {
            // THESE ARE VALID HOURS SO KEEP THEM
            // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
            startHour = initStartHour;
            endHour = initEndHour;
        }
        resetCSG();
    }
    
    public void addTA(TeachingAssistantPrototype ta) {
        if (!hasTA(ta)) {
            TAType taType = TAType.valueOf(ta.getType());
            ArrayList<TeachingAssistantPrototype> tas = allTAs.get(taType);
            tas.add(ta);
            this.updateTAs();
        }
    }

    public void addTA(TeachingAssistantPrototype ta, HashMap<TimeSlot, ArrayList<DayOfWeek>> officeHours) {
        addTA(ta);
        for (TimeSlot timeSlot : officeHours.keySet()) {
            ArrayList<DayOfWeek> days = officeHours.get(timeSlot);
            for (DayOfWeek dow : days) {
                timeSlot.addTA(dow, ta);
            }
        }
    }
    
    public void removeTA(TeachingAssistantPrototype ta) {
        // REMOVE THE TA FROM THE LIST OF TAs
        TAType taType = TAType.valueOf(ta.getType());
        allTAs.get(taType).remove(ta);
        
        // REMOVE THE TA FROM ALL OF THEIR OFFICE HOURS
        for (TimeSlot timeSlot : officeHours) {
            timeSlot.removeTA(ta);
        }
        
        // AND REFRESH THE TABLES
        this.updateTAs();
    }

    public void removeTA(TeachingAssistantPrototype ta, HashMap<TimeSlot, ArrayList<DayOfWeek>> officeHours) {
        removeTA(ta);
        for (TimeSlot timeSlot : officeHours.keySet()) {
            ArrayList<DayOfWeek> days = officeHours.get(timeSlot);
            for (DayOfWeek dow : days) {
                timeSlot.removeTA(dow, ta);
            }
        }    
    }
    
    public DayOfWeek getColumnDayOfWeek(int columnNumber) {
        return TimeSlot.DayOfWeek.values()[columnNumber-2];
    }

    public TeachingAssistantPrototype getTAWithName(String name) {
        Iterator<TeachingAssistantPrototype> taIterator = teachingAssistants.iterator();
        while (taIterator.hasNext()) {
            TeachingAssistantPrototype ta = taIterator.next();
            if (ta.getName().equals(name))
                return ta;
        }
        return null;
    }

    public TeachingAssistantPrototype getTAWithEmail(String email) {
        Iterator<TeachingAssistantPrototype> taIterator = teachingAssistants.iterator();
        while (taIterator.hasNext()) {
            TeachingAssistantPrototype ta = taIterator.next();
            if (ta.getEmail().equals(email))
                return ta;
        }
        return null;
    }

    public TimeSlot getTimeSlot(String startTime) {
        Iterator<TimeSlot> timeSlotsIterator = officeHours.iterator();
        while (timeSlotsIterator.hasNext()) {
            TimeSlot timeSlot = timeSlotsIterator.next();
            String timeSlotStartTime = timeSlot.getStartTime().replace(":", "_");
            if (timeSlotStartTime.equals(startTime))
                return timeSlot;
        }
        return null;
    }

    public TAType getSelectedType() {
        RadioButton allRadio = (RadioButton)app.getGUIModule().getGUINode(CSG_OFFICE_HOURS_ALL_RADIO_BUTTON);
        if (allRadio.isSelected())
            return TAType.All;
        RadioButton gradRadio = (RadioButton)app.getGUIModule().getGUINode(CSG_OFFICE_HOURS_GRAD_RADIO_BUTTON);
        if (gradRadio.isSelected())
            return TAType.Graduate;
        else
            return TAType.Undergraduate;
    }

    public TeachingAssistantPrototype getSelectedTA() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TeachingAssistantPrototype> tasTable = (TableView)gui.getGUINode(CSG_OFFICE_HOURS_TAS_TABLE_VIEW);
        return tasTable.getSelectionModel().getSelectedItem();
    }
    
    public HashMap<TimeSlot, ArrayList<DayOfWeek>> getTATimeSlots(TeachingAssistantPrototype ta) {
        HashMap<TimeSlot, ArrayList<DayOfWeek>> timeSlots = new HashMap();
        for (TimeSlot timeSlot : officeHours) {
            if (timeSlot.hasTA(ta)) {
                ArrayList<DayOfWeek> daysForTA = timeSlot.getDaysForTA(ta);
                timeSlots.put(timeSlot, daysForTA);
            }
        }
        return timeSlots;
    }
    
    private boolean hasTA(TeachingAssistantPrototype testTA) {
        return allTAs.get(TAType.Graduate).contains(testTA)
                ||
                allTAs.get(TAType.Undergraduate).contains(testTA);
    }
    
    public boolean isTASelected() {
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView)gui.getGUINode(CSG_OFFICE_HOURS_TAS_TABLE_VIEW);
        return tasTable.getSelectionModel().getSelectedItem() != null;
    }

    public boolean isLegalNewTA(String name, String email) {
        if ((name.trim().length() > 0)
                && (email.trim().length() > 0)) {
            // MAKE SURE NO TA ALREADY HAS THE SAME NAME
            TAType type = this.getSelectedType();
            TeachingAssistantPrototype testTA = new TeachingAssistantPrototype(name, email, type);
            if (this.teachingAssistants.contains(testTA))
                return false;
            if (this.isLegalNewEmail(email)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isLegalNewName(String testName) {
        if (testName.trim().length() > 0) {
            for (TeachingAssistantPrototype testTA : this.teachingAssistants) {
                if (testTA.getName().equals(testName))
                    return false;
            }
            return true;
        }
        return false;
    }
    
    public boolean isLegalNewEmail(String email) {
        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
                "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        if (matcher.find()) {
            for (TeachingAssistantPrototype ta : this.teachingAssistants) {
                if (ta.getEmail().equals(email.trim()))
                    return false;
            }
            return true;
        }
        else return false;
    }
    
    public boolean isDayOfWeekColumn(int columnNumber) {
        return columnNumber >= 2;
    }
    
    public boolean isTATypeSelected() {
        AppGUIModule gui = app.getGUIModule();
        RadioButton allRadioButton = (RadioButton)gui.getGUINode(CSG_OFFICE_HOURS_ALL_RADIO_BUTTON);
        return !allRadioButton.isSelected();
    }
    
    public boolean isValidTAEdit(TeachingAssistantPrototype taToEdit, String name, String email) {
        if (!taToEdit.getName().equals(name)) {
            if (!this.isLegalNewName(name))
                return false;
        }
        if (!taToEdit.getEmail().equals(email)) {
            if (!this.isLegalNewEmail(email))
                return false;
        }
        return true;
    }

    public boolean isValidNameEdit(TeachingAssistantPrototype taToEdit, String name) {
        if (!taToEdit.getName().equals(name)) {
            if (!this.isLegalNewName(name))
                return false;
        }
        return true;
    }

    public boolean isValidEmailEdit(TeachingAssistantPrototype taToEdit, String email) {
        if (!taToEdit.getEmail().equals(email)) {
            if (!this.isLegalNewEmail(email))
                return false;
        }
        return true;
    }    

    public void updateTAs() {
        TAType type = getSelectedType();
        selectTAs(type);
    }
    
    public void selectTAs(TAType type) {
        teachingAssistants.clear();
        Iterator<TeachingAssistantPrototype> tasIt = this.teachingAssistantsIterator();
        while (tasIt.hasNext()) {
            TeachingAssistantPrototype ta = tasIt.next();
            if (type.equals(TAType.All)) {
                teachingAssistants.add(ta);
            }
            else if (ta.getType().equals(type.toString())) {
                teachingAssistants.add(ta);
            }
        }
        
        // SORT THEM BY NAME
        sortTAs();

        // CLEAR ALL THE OFFICE HOURS
        Iterator<TimeSlot> officeHoursIt = officeHours.iterator();
        while (officeHoursIt.hasNext()) {
            TimeSlot timeSlot = officeHoursIt.next();
            timeSlot.filter(type);
        }
        
        app.getFoolproofModule().updateAll();
    }
    
    /* returns a long based on the OH native time string format */
    private long _timestring2long(String ts) {
        long l = -1L;
        try { // convert to long format
            l = new SimpleDateFormat("hh:mmaa").parse(ts).toInstant().getEpochSecond();
        } catch (ParseException e) {
            
        } catch (UnsupportedOperationException e) {
            
        }
        return l;
    }
    
    public void setTimeRange(long startTimeAs_time_t, long endTimeAs_time_t) {
        officeHours.clear();
        long ts;
        for (TimeSlot slot : allSlots) {
            ts = _timestring2long(slot.getStartTime());
            if (ts >= startTimeAs_time_t && ts < endTimeAs_time_t)
                officeHours.add(slot);
        }
        app.getFoolproofModule().updateAll();
    }
    
    public void editPageDetails() {
        editPageDetails(null, null);
    }
    /* DOESN'T VALIDATE INSTRUCTOR EMAIL, WEBSITE, NOR OFFICE HOURS */
    public void editPageDetails(Object chooserButtonID, String path) {
        AppGUIModule gui = app.getGUIModule();
        pageDetails.setSubject((String) ((ComboBox) gui.getGUINode(
                CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SUBJECT_HBOX_COMBOBOX)).getSelectionModel().getSelectedItem());
        pageDetails.setNumber(Integer.parseInt((String) ((ComboBox) gui.getGUINode(
                CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_NUMBER_HBOX_COMBOBOX)).getSelectionModel().getSelectedItem()));
        pageDetails.setSemester((String) ((ComboBox) gui.getGUINode(
                CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SEMESTER_HBOX_COMBOBOX)).getSelectionModel().getSelectedItem());
        pageDetails.setYear(Integer.parseInt((String) ((ComboBox) gui.getGUINode(
                CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_YEAR_HBOX_COMBOBOX)).getSelectionModel().getSelectedItem()));
        
        pageDetails.setTitle((String) ((TextField) gui.getGUINode(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_TITLE_HBOX_TEXT_FIELD)).getText());
        pageDetails.setExportDir("./export/" + pageDetails.getSubject() + "_" + pageDetails.getNumber() + "_" + pageDetails.getSemester() + "_" + pageDetails.getYear() + "/");
        
        pageDetails.setIncludeHomePage(((CheckBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_PAGES_HBOX_HOME_CHECKBOX)).isSelected());
        pageDetails.setIncludeHWsPage(((CheckBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_PAGES_HBOX_HWS_CHECKBOX)).isSelected());
        pageDetails.setIncludeSchedulePage(((CheckBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_PAGES_HBOX_SCHEDULE_CHECKBOX)).isSelected());
        pageDetails.setIncludeSyllabusPage(((CheckBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_PAGES_HBOX_SYLLABUS_CHECKBOX)).isSelected());
        
        if (chooserButtonID == null || path == null)
            ;
        else if (chooserButtonID.toString().equals(CSG_SITE_SPLIT_PANE_STYLE_VBOX_FAVICON_HBOX_BUTTON.toString()))
            pageDetails.setFaviconPath(path);
        else if (chooserButtonID.toString().equals(CSG_SITE_SPLIT_PANE_STYLE_VBOX_NAVBAR_HBOX_BUTTON.toString()))
            pageDetails.setNavbarPath(path);
        else if (chooserButtonID.toString().equals(CSG_SITE_SPLIT_PANE_STYLE_VBOX_LEFT_FOOTER_HBOX_BUTTON.toString()))
            pageDetails.setLeftFooterPath(path);
        else if (chooserButtonID.toString().equals(CSG_SITE_SPLIT_PANE_STYLE_VBOX_RIGHT_FOOTER_HBOX_BUTTON.toString()))
            pageDetails.setRightFooterPath(path);
        
        pageDetails.setStylesheetPath((String) ((ComboBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_STYLE_VBOX_STYLESHEET_HBOX_CHOOSER)).getSelectionModel().getSelectedItem());
        updatePageDetails(chooserButtonID, path); // must happen here
    }
    
    // update the GUI
    public void updatePageDetails() {
        updatePageDetails(null, null);
    }
    public void updatePageDetails(Object chooserButtonID, String imagePath) {
        try {AppGUIModule gui = app.getGUIModule();
        ((ComboBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SUBJECT_HBOX_COMBOBOX)).getSelectionModel().select(pageDetails.getSubject());
        ((ComboBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_NUMBER_HBOX_COMBOBOX)).getSelectionModel().select((new Integer(pageDetails.getNumber()).toString()));
        ((ComboBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_SEMESTER_HBOX_COMBOBOX)).getSelectionModel().select(pageDetails.getSemester());
        ((ComboBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_GRID_PANE_YEAR_HBOX_COMBOBOX)).getSelectionModel().select((new Integer(pageDetails.getYear()).toString()));
        
        TextField tf = ((TextField) gui.getGUINode(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_TITLE_HBOX_TEXT_FIELD));
        tf.setText(pageDetails.getTitle());
        tf.positionCaret(tf.getText().length());
        
        ((Label) gui.getGUINode(CSG_SITE_SPLIT_PANE_BANNER_VBOX_VBOX_EXPORT_DIR_HBOX_PATH_LABEL)).setText(pageDetails.getExportDir());
        
        ((CheckBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_PAGES_HBOX_HOME_CHECKBOX)).setSelected(pageDetails.getIncludeHomePage());
        ((CheckBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_PAGES_HBOX_HWS_CHECKBOX)).setSelected(pageDetails.getIncludeHWsPage());
        ((CheckBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_PAGES_HBOX_SCHEDULE_CHECKBOX)).setSelected(pageDetails.getIncludeSchedulePage());
        ((CheckBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_PAGES_HBOX_SYLLABUS_CHECKBOX)).setSelected(pageDetails.getIncludeSyllabusPage());
        
        imagePath = "file:" + imagePath; // wrap image's url requirement
        
        if (chooserButtonID == null || imagePath == null)
        ;
        else if (chooserButtonID.toString().equals(CSG_SITE_SPLIT_PANE_STYLE_VBOX_FAVICON_HBOX_BUTTON.toString()))
            ((ImageView) gui.getGUINode(CSG_SITE_SPLIT_PANE_STYLE_VBOX_FAVICON_HBOX_IMAGE)).setImage(new Image(imagePath));
        else if (chooserButtonID.toString().equals(CSG_SITE_SPLIT_PANE_STYLE_VBOX_NAVBAR_HBOX_BUTTON.toString()))
            ((ImageView) gui.getGUINode(CSG_SITE_SPLIT_PANE_STYLE_VBOX_NAVBAR_HBOX_IMAGE)).setImage(new Image(imagePath));
        else if (chooserButtonID.toString().equals(CSG_SITE_SPLIT_PANE_STYLE_VBOX_LEFT_FOOTER_HBOX_BUTTON.toString()))
            ((ImageView) gui.getGUINode(CSG_SITE_SPLIT_PANE_STYLE_VBOX_LEFT_FOOTER_HBOX_IMAGE)).setImage(new Image(imagePath));
        else if (chooserButtonID.toString().equals(CSG_SITE_SPLIT_PANE_STYLE_VBOX_RIGHT_FOOTER_HBOX_BUTTON.toString()))
            ((ImageView) gui.getGUINode(CSG_SITE_SPLIT_PANE_STYLE_VBOX_RIGHT_FOOTER_HBOX_IMAGE)).setImage(new Image(imagePath));
        
        ComboBox box = (ComboBox) gui.getGUINode(CSG_SITE_SPLIT_PANE_STYLE_VBOX_STYLESHEET_HBOX_CHOOSER);
        box.getItems().clear();
        
        for (java.io.File f : (new java.io.File("./work/style/").listFiles()))
                box.getItems().add(f.toString());
        box.getSelectionModel().select(pageDetails.getStylesheetPath());
        } catch (NullPointerException e) {
            
        }
    }
    
    public PageDetails getPageDetails() {
        return pageDetails;
    }
    
    public void editInstructorDetails() {
        AppGUIModule gui = app.getGUIModule();
        instructorDetails.setName(((TextField) gui.getGUINode(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_NAME_HBOX_TEXT_FIELD)).getText());
        instructorDetails.setRoom(((TextField) gui.getGUINode(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_ROOM_HBOX_TEXT_FIELD)).getText());
        instructorDetails.setEmail(((TextField) gui.getGUINode(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_EMAIL_HBOX_TEXT_FIELD)).getText());
        instructorDetails.setWebsite(((TextField) gui.getGUINode(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_WEBSITE_HBOX_TEXT_FIELD)).getText());
        
        instructorDetails.setOfficeHours(((TextArea) gui.getGUINode(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_OFFICE_HOURS_VBOX_TEXT_AREA)).getText());
    }
    
    public void updateInstructorDetails() {
        AppGUIModule gui = app.getGUIModule();
        
        TextField tf = ((TextField) gui.getGUINode(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_NAME_HBOX_TEXT_FIELD));
        tf.setText(instructorDetails.getName());
        tf.positionCaret(tf.getText().length());
        
        tf = ((TextField) gui.getGUINode(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_ROOM_HBOX_TEXT_FIELD));
        tf.setText(instructorDetails.getRoom());
        tf.positionCaret(tf.getText().length());
        
        tf = ((TextField) gui.getGUINode(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_EMAIL_HBOX_TEXT_FIELD));
        tf.setText(instructorDetails.getEmail());
        tf.positionCaret(tf.getText().length());
        
        tf = ((TextField) gui.getGUINode(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_GRID_PANE_WEBSITE_HBOX_TEXT_FIELD));
        tf.setText(instructorDetails.getWebsite());
        tf.positionCaret(tf.getText().length());
        
        ((TextArea) gui.getGUINode(CSG_SITE_SPLIT_PANE_INSTRUCTOR_VBOX_OFFICE_HOURS_VBOX_TEXT_AREA)).setText(instructorDetails.getOfficeHours());
    }
    
    public InstructorDetails getInstructorDetails() {
        return instructorDetails;
    }
    
    public void editSyllabusDetails() {
        AppGUIModule gui = app.getGUIModule();
        
        syllabusDetails.setDescription(((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_DESCRIPTION_VBOX_TEXT_AREA)).getText());
        syllabusDetails.setTopics(((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_TOPICS_VBOX_TEXT_AREA)).getText());
        syllabusDetails.setPrereqs(((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_PREREQS_VBOX_TEXT_AREA)).getText());
        syllabusDetails.setOutcomes(((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_OUTCOMES_VBOX_TEXT_AREA)).getText());
        syllabusDetails.setTextbooks(((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_TEXTBOOKS_VBOX_TEXT_AREA)).getText());
        syllabusDetails.setGradedComponents(((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_GRADED_COMPONENTS_VBOX_TEXT_AREA)).getText());
        syllabusDetails.setGradingNotes(((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_GRADING_NOTES_VBOX_TEXT_AREA)).getText());
        syllabusDetails.setAcademicDishonesty(((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_ACADEMIC_DISHONESTY_VBOX_TEXT_AREA)).getText());
        syllabusDetails.setSpecialAssistance(((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_SPECIAL_ASSISTANCE_VBOX_TEXT_AREA)).getText());
    }
    
    public void updateSyllabusDetails() {
        AppGUIModule gui = app.getGUIModule();
        
        ((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_DESCRIPTION_VBOX_TEXT_AREA)).setText(syllabusDetails.getDescription());
        ((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_TOPICS_VBOX_TEXT_AREA)).setText(syllabusDetails.getTopics());
        ((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_PREREQS_VBOX_TEXT_AREA)).setText(syllabusDetails.getPrereqs());
        ((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_OUTCOMES_VBOX_TEXT_AREA)).setText(syllabusDetails.getOutcomes());
        ((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_TEXTBOOKS_VBOX_TEXT_AREA)).setText( syllabusDetails.getTextbooks());
        ((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_GRADED_COMPONENTS_VBOX_TEXT_AREA)).setText(syllabusDetails.getGradedComponents());
        ((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_GRADING_NOTES_VBOX_TEXT_AREA)).setText(syllabusDetails.getGradingNotes());
        ((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_ACADEMIC_DISHONESTY_VBOX_TEXT_AREA)).setText(syllabusDetails.getAcademicDishonesty());
        ((TextArea) gui.getGUINode( CSG_SYLLABUS_SPLIT_PANE_SPECIAL_ASSISTANCE_VBOX_TEXT_AREA)).setText(syllabusDetails.getSpecialAssistance());
    }
    
    public SyllabusDetails getSyllabusDetails() {
        return syllabusDetails;
    }
    
    public Iterator<TimeSlot> officeHoursIterator() {
        return officeHours.iterator();
    }

    public Iterator<TeachingAssistantPrototype> teachingAssistantsIterator() {
        return new AllTAsIterator();
    }
    
    private class AllTAsIterator implements Iterator {
        Iterator gradIt = allTAs.get(TAType.Graduate).iterator();
        Iterator undergradIt = allTAs.get(TAType.Undergraduate).iterator();

        public AllTAsIterator() {}
        
        @Override
        public boolean hasNext() {
            if (gradIt.hasNext() || undergradIt.hasNext())
                return true;
            else
                return false;                
        }

        @Override
        public Object next() {
            if (gradIt.hasNext())
                return gradIt.next();
            else if (undergradIt.hasNext())
                return undergradIt.next();
            else
                return null;
        }
    }
    public void addLecture(LectureDetails lecture) {
        if (lecture == null)
            return;
        lectureDetails.add(lecture);
    }
    public void removeLecture(LectureDetails lecture) {
        if (lecture != null)
            lectureDetails.remove(lecture);
    }
    public void addRecitation(RecitationDetails o) {
        if (o == null)
            return;
        recitationDetails.add(o);
    }
    public void removeRecitation(RecitationDetails o) {
        if (o != null)
            recitationDetails.remove(o);
    }
    public void addLab(LabDetails o) {
        if (o == null)
            return;
        labDetails.add(o);
    }
    public void removeLab(LabDetails o) {
        if (o != null)
            labDetails.remove(o);
    }
    public ObservableList<LectureDetails> getLectures() {
        return lectureDetails;
    }
    public ObservableList<RecitationDetails> getRecitations() {
        return recitationDetails;
    }
    public ObservableList<LabDetails> getLabs() {
        return labDetails;
    }
    public void addScheduleItem(ScheduleItemPrototype o) {
        if (o != null)
            scheduleItems.add(o);
    }
    public void removeScheduleItem(ScheduleItemPrototype o) {
        scheduleItems.remove(o);
    }
    public ObservableList<ScheduleItemPrototype> getScheduleItems() {
        return scheduleItems;
    }
    public void updateStartEndDates() {
        AppGUIModule gui = app.getGUIModule();
        ((DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_START_DATE_PICKER)).setValue(java.time.LocalDate.parse(startingMonday));
        ((DatePicker) gui.getGUINode(CSG_SCHEDULE_SPLIT_PANE_CALENDAR_BOUNDS_HBOX_END_DATE_PICKER)).setValue(java.time.LocalDate.parse(endingFriday));
    }
}
