#readme
In addition to my genetic predisposition to hate modern web browsing technologies, I started this portion of the assignment late.
Another major issue was the slight differences between my JSON data and the ones we were so subtly told to use; I wasted a good chunk of my time altering the JS.
You may notice that there's a transactional issue with editing rows in the meeting times tab: the data structure is there, but it won't fire.
The issue is triggering it, which I haven't quite gotten.
##author
If you're **actually** interested in my *real* work, visit [my site](bdefino.github.io).
I'm interested in making and breaking systems.
Expect my pure-C SOCKS5 library (with a sweet chaining extension) to be published around New Year's.
